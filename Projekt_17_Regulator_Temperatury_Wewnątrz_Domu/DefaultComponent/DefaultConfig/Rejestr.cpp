/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Rejestr
//!	Generated Date	: Tue, 10, Sep 2019  
	File Path	: DefaultComponent/DefaultConfig/Rejestr.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Rejestr.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define Default_Rejestr_Rejestr_SERIALIZE OM_NO_OP

#define Default_Rejestr_evReplyRejestr_SERIALIZE OM_NO_OP

#define Default_Rejestr_getOption_SERIALIZE OM_NO_OP

#define Default_Rejestr_off_SERIALIZE OM_NO_OP

#define Default_Rejestr_on_SERIALIZE OM_NO_OP

#define Default_Rejestr_setOption_SERIALIZE aomsmethod->addAttribute("option", x2String(option));
//#]

//## package Default

//## class Rejestr
Rejestr::Rejestr(IOxfActive* theActiveContext) : CzasR(0), Zdarzenie(0) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Rejestr, Rejestr(), 0, Default_Rejestr_Rejestr_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsSterownik = NULL;
}

Rejestr::~Rejestr() {
    NOTIFY_DESTRUCTOR(~Rejestr, false);
    cleanUpRelations();
}

void Rejestr::evReplyRejestr() {
    NOTIFY_OPERATION(evReplyRejestr, evReplyRejestr(), 0, Default_Rejestr_evReplyRejestr_SERIALIZE);
    //#[ operation evReplyRejestr()
    //#]
}

RhpString Rejestr::getOption() {
    NOTIFY_OPERATION(getOption, getOption(), 0, Default_Rejestr_getOption_SERIALIZE);
    //#[ operation getOption()
    return "x";
    //#]
}

bool Rejestr::off() {
    NOTIFY_OPERATION(off, off(), 0, Default_Rejestr_off_SERIALIZE);
    //#[ operation off()
    return true;
    //#]
}

bool Rejestr::on() {
    NOTIFY_OPERATION(on, on(), 0, Default_Rejestr_on_SERIALIZE);
    //#[ operation on()
    return true;
    //#]
}

bool Rejestr::setOption(const RhpString& option) {
    NOTIFY_OPERATION(setOption, setOption(const RhpString&), 1, Default_Rejestr_setOption_SERIALIZE);
    //#[ operation setOption(RhpString)
    return true;
    //#]
}

Sterownik* Rejestr::getItsSterownik() const {
    return itsSterownik;
}

void Rejestr::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool Rejestr::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void Rejestr::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

int Rejestr::getCzasR() const {
    return CzasR;
}

void Rejestr::setCzasR(int p_CzasR) {
    CzasR = p_CzasR;
}

int Rejestr::getZdarzenie() const {
    return Zdarzenie;
}

void Rejestr::setZdarzenie(int p_Zdarzenie) {
    Zdarzenie = p_Zdarzenie;
}

void Rejestr::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void Rejestr::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void Rejestr::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedRejestr::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("CzasR", x2String(myReal->CzasR));
    aomsAttributes->addAttribute("Zdarzenie", x2String(myReal->Zdarzenie));
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedRejestr::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_REACTIVE_META_S_SIMPLE_P(Rejestr, Default, false, Modul, OMAnimatedModul, OMAnimatedRejestr)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Rejestr.cpp
*********************************************************************/
