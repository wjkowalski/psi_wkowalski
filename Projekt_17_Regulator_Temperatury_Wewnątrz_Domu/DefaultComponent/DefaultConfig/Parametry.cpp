/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Parametry
//!	Generated Date	: Tue, 10, Sep 2019  
	File Path	: DefaultComponent/DefaultConfig/Parametry.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Parametry.h"
//## link itsRegulator
#include "Regulator.h"
//#[ ignore
#define Default_Parametry_Parametry_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Parametry
Parametry::Parametry() {
    NOTIFY_CONSTRUCTOR(Parametry, Parametry(), 0, Default_Parametry_Parametry_SERIALIZE);
    itsRegulator = NULL;
}

Parametry::~Parametry() {
    NOTIFY_DESTRUCTOR(~Parametry, true);
    cleanUpRelations();
}

Regulator* Parametry::getItsRegulator() const {
    return itsRegulator;
}

void Parametry::setItsRegulator(Regulator* p_Regulator) {
    _setItsRegulator(p_Regulator);
}

void Parametry::cleanUpRelations() {
    if(itsRegulator != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsRegulator");
            itsRegulator = NULL;
        }
}

void Parametry::__setItsRegulator(Regulator* p_Regulator) {
    itsRegulator = p_Regulator;
    if(p_Regulator != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsRegulator", p_Regulator, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsRegulator");
        }
}

void Parametry::_setItsRegulator(Regulator* p_Regulator) {
    __setItsRegulator(p_Regulator);
}

void Parametry::_clearItsRegulator() {
    NOTIFY_RELATION_CLEARED("itsRegulator");
    itsRegulator = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedParametry::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsRegulator", false, true);
    if(myReal->itsRegulator)
        {
            aomsRelations->ADD_ITEM(myReal->itsRegulator);
        }
}
//#]

IMPLEMENT_META_P(Parametry, Default, Default, false, OMAnimatedParametry)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Parametry.cpp
*********************************************************************/
