/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Grzejnik
//!	Generated Date	: Tue, 10, Sep 2019  
	File Path	: DefaultComponent/DefaultConfig/Grzejnik.h
*********************************************************************/

#ifndef Grzejnik_H
#define Grzejnik_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Grzejnik
#include "Modul.h"
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include <oxf/omthread.h>
//#[ ignore
#define OMAnim_Default_Grzejnik_setStanG_Stan_ARGS_DECLARATION Stan p_StanG;
//#]

//## link itsSterownik
class Sterownik;

//## package Default

//## class Grzejnik
class Grzejnik : public OMReactive, public Modul {
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedGrzejnik;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Grzejnik(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    virtual ~Grzejnik();
    
    ////    Operations    ////
    
    //## operation getOption()
    virtual RhpString getOption();
    
    //## operation off()
    virtual bool off();
    
    //## operation on()
    virtual bool on();
    
    //## operation setOption(RhpString)
    virtual bool setOption(const RhpString& option);
    
    ////    Additional operations    ////
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    void setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();

private :

    //## auto_generated
    Stan getStanG() const;

public :

    //## auto_generated
    void setStanG(Stan p_StanG);

private :

    //## auto_generated
    int getTempZadanaG() const;
    
    //## auto_generated
    void setTempZadanaG(int p_TempZadanaG);
    
    ////    Attributes    ////

protected :

    Stan StanG;		//## attribute StanG
    
    int TempZadanaG;		//## attribute TempZadanaG
    
    ////    Relations and components    ////
    
    Sterownik* itsSterownik;		//## link itsSterownik
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _clearItsSterownik();
    
    ////    Framework    ////

private :

    //## auto_generated
    int getTempBiezG() const;
    
    //## auto_generated
    void setTempBiezG(int p_TempBiezG);

protected :

    int TempBiezG;		//## attribute TempBiezG

public :

    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // sendaction_6:
    //## statechart_method
    inline bool sendaction_6_IN() const;
    
    // sendaction_5:
    //## statechart_method
    inline bool sendaction_5_IN() const;
    
    // sendaction_4:
    //## statechart_method
    inline bool sendaction_4_IN() const;
    
    // ON:
    //## statechart_method
    inline bool ON_IN() const;
    
    // OFF:
    //## statechart_method
    inline bool OFF_IN() const;

protected :

//#[ ignore
    enum Grzejnik_Enum {
        OMNonState = 0,
        sendaction_6 = 1,
        sendaction_5 = 2,
        sendaction_4 = 3,
        ON = 4,
        OFF = 5
    };
    
    int rootState_subState;
    
    int rootState_active;
//#]
};

#ifdef _OMINSTRUMENT
DECLARE_OPERATION_CLASS(Default_Grzejnik_setStanG_Stan)

//#[ ignore
class OMAnimatedGrzejnik : public OMAnimatedModul {
    DECLARE_REACTIVE_META(Grzejnik, OMAnimatedGrzejnik)
    
    DECLARE_META_OP(Default_Grzejnik_setStanG_Stan)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_6_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_5_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_4_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void ON_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void OFF_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Grzejnik::rootState_IN() const {
    return true;
}

inline bool Grzejnik::sendaction_6_IN() const {
    return rootState_subState == sendaction_6;
}

inline bool Grzejnik::sendaction_5_IN() const {
    return rootState_subState == sendaction_5;
}

inline bool Grzejnik::sendaction_4_IN() const {
    return rootState_subState == sendaction_4;
}

inline bool Grzejnik::ON_IN() const {
    return rootState_subState == ON;
}

inline bool Grzejnik::OFF_IN() const {
    return rootState_subState == OFF;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Grzejnik.h
*********************************************************************/
