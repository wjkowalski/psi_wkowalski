/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Regulator
//!	Generated Date	: Tue, 10, Sep 2019  
	File Path	: DefaultComponent/DefaultConfig/Regulator.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Regulator.h"
//#[ ignore
#define Default_Regulator_Regulator_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Regulator
Regulator::Regulator(IOxfActive* theActiveContext) : tryb(0) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Regulator, Regulator(), 0, Default_Regulator_Regulator_SERIALIZE);
    setActiveContext(theActiveContext, false);
    {
        {
            itsSterownik.setShouldDelete(false);
        }
    }
    initRelations();
    initStatechart();
}

Regulator::~Regulator() {
    NOTIFY_DESTRUCTOR(~Regulator, true);
    cancelTimeouts();
}

Sterownik* Regulator::getItsSterownik() const {
    return (Sterownik*) &itsSterownik;
}

bool Regulator::startBehavior() {
    bool done = true;
    done &= itsSterownik.startBehavior();
    done &= OMReactive::startBehavior();
    return done;
}

void Regulator::initRelations() {
    itsSterownik._setItsRegulator(this);
}

void Regulator::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
    RecznaZmianaUp_subState = OMNonState;
    RecznaZmianaUp_timeout = NULL;
    RecznaZmianaDown_subState = OMNonState;
    RecznaZmianaDown_timeout = NULL;
    AutoZmiana_subState = OMNonState;
    AutoZmiana_timeout = NULL;
}

void Regulator::cancelTimeouts() {
    cancel(RecznaZmianaUp_timeout);
    cancel(RecznaZmianaDown_timeout);
    cancel(AutoZmiana_timeout);
}

bool Regulator::cancelTimeout(const IOxfTimeout* arg) {
    bool res = false;
    if(RecznaZmianaUp_timeout == arg)
        {
            RecznaZmianaUp_timeout = NULL;
            res = true;
        }
    if(RecznaZmianaDown_timeout == arg)
        {
            RecznaZmianaDown_timeout = NULL;
            res = true;
        }
    if(AutoZmiana_timeout == arg)
        {
            AutoZmiana_timeout = NULL;
            res = true;
        }
    return res;
}

char Regulator::getTryb() const {
    return tryb;
}

void Regulator::setTryb(char p_tryb) {
    tryb = p_tryb;
}

void Regulator::setActiveContext(IOxfActive* theActiveContext, bool activeInstance) {
    OMReactive::setActiveContext(theActiveContext, activeInstance);
    {
        itsSterownik.setActiveContext(theActiveContext, false);
    }
}

void Regulator::destroy() {
    itsSterownik.destroy();
    OMReactive::destroy();
}

void Regulator::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
        rootState_subState = Oczekiwanie;
        rootState_active = Oczekiwanie;
        //#[ state Oczekiwanie.(Entry) 
                      //cout<<endl<<"Oczekiwanie";
                      //cout<<endl<<"Dostepne tryby:";  
                      //cout<<endl<<"[W]yswietl rejestr";
                      //cout<<endl<<"[A]utomatyczna zmiana temperatury";
                      //cout<<endl<<"[G]Zwiekszenie temperatury";
                      //cout<<endl<<"[C]Zmniejszenie temperatury";
        //#]
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Regulator::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Oczekiwanie
        case Oczekiwanie:
        {
            res = Oczekiwanie_handleEvent();
        }
        break;
        // State Awaria
        case Awaria:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("17");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Awaria");
                    NOTIFY_STATE_ENTERED("ROOT.terminationstate_30");
                    rootState_subState = terminationstate_30;
                    rootState_active = terminationstate_30;
                    NOTIFY_TRANSITION_TERMINATED("17");
                    res = eventConsumed;
                }
            
        }
        break;
        // State SprawdzenieTemp
        case SprawdzenieTemp:
        {
            if(IS_EVENT_TYPE_OF(evZadajTemp_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("13");
                    NOTIFY_STATE_EXITED("ROOT.RecznaZmianaUp.SprawdzenieTemp");
                    NOTIFY_STATE_ENTERED("ROOT.RecznaZmianaUp.Wlaczenie");
                    RecznaZmianaUp_subState = Wlaczenie;
                    rootState_active = Wlaczenie;
                    NOTIFY_TRANSITION_TERMINATED("13");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State Wlaczenie
        case Wlaczenie:
        {
            if(IS_EVENT_TYPE_OF(evStart_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("27");
                    NOTIFY_STATE_EXITED("ROOT.RecznaZmianaUp.Wlaczenie");
                    NOTIFY_STATE_ENTERED("ROOT.RecznaZmianaUp.Grzanie");
                    RecznaZmianaUp_subState = Grzanie;
                    rootState_active = Grzanie;
                    RecznaZmianaUp_timeout = scheduleTimeout(600, "ROOT.RecznaZmianaUp.Grzanie");
                    NOTIFY_TRANSITION_TERMINATED("27");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State Grzanie
        case Grzanie:
        {
            if(IS_EVENT_TYPE_OF(evStop_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("25");
                    cancel(RecznaZmianaUp_timeout);
                    NOTIFY_STATE_EXITED("ROOT.RecznaZmianaUp.Grzanie");
                    NOTIFY_STATE_ENTERED("ROOT.RecznaZmianaUp.Wylaczenie");
                    RecznaZmianaUp_subState = Wylaczenie;
                    rootState_active = Wylaczenie;
                    RecznaZmianaUp_timeout = scheduleTimeout(0, "ROOT.RecznaZmianaUp.Wylaczenie");
                    NOTIFY_TRANSITION_TERMINATED("25");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == RecznaZmianaUp_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("24");
                            cancel(RecznaZmianaUp_timeout);
                            NOTIFY_STATE_EXITED("ROOT.RecznaZmianaUp.Grzanie");
                            NOTIFY_STATE_ENTERED("ROOT.RecznaZmianaUp.SprawdzenieTemp");
                            RecznaZmianaUp_subState = SprawdzenieTemp;
                            rootState_active = SprawdzenieTemp;
                            NOTIFY_TRANSITION_TERMINATED("24");
                            res = eventConsumed;
                        }
                }
            
            
        }
        break;
        // State Wylaczenie
        case Wylaczenie:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == RecznaZmianaUp_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("35");
                            RecznaZmianaUp_exit();
                            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                            rootState_subState = Oczekiwanie;
                            rootState_active = Oczekiwanie;
                            //#[ state Oczekiwanie.(Entry) 
                                          //cout<<endl<<"Oczekiwanie";
                                          //cout<<endl<<"Dostepne tryby:";  
                                          //cout<<endl<<"[W]yswietl rejestr";
                                          //cout<<endl<<"[A]utomatyczna zmiana temperatury";
                                          //cout<<endl<<"[G]Zwiekszenie temperatury";
                                          //cout<<endl<<"[C]Zmniejszenie temperatury";
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("35");
                            res = eventConsumed;
                        }
                }
            
            
        }
        break;
        // State Chlodzenie
        case Chlodzenie:
        {
            if(IS_EVENT_TYPE_OF(evStop_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("22");
                    cancel(RecznaZmianaDown_timeout);
                    NOTIFY_STATE_EXITED("ROOT.RecznaZmianaDown.Chlodzenie");
                    NOTIFY_STATE_ENTERED("ROOT.RecznaZmianaDown.Wylaczenie");
                    RecznaZmianaDown_subState = RecznaZmianaDown_Wylaczenie;
                    rootState_active = RecznaZmianaDown_Wylaczenie;
                    RecznaZmianaDown_timeout = scheduleTimeout(0, "ROOT.RecznaZmianaDown.Wylaczenie");
                    NOTIFY_TRANSITION_TERMINATED("22");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == RecznaZmianaDown_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("23");
                            cancel(RecznaZmianaDown_timeout);
                            NOTIFY_STATE_EXITED("ROOT.RecznaZmianaDown.Chlodzenie");
                            NOTIFY_STATE_ENTERED("ROOT.RecznaZmianaDown.SprawdzenieTemp");
                            RecznaZmianaDown_subState = RecznaZmianaDown_SprawdzenieTemp;
                            rootState_active = RecznaZmianaDown_SprawdzenieTemp;
                            NOTIFY_TRANSITION_TERMINATED("23");
                            res = eventConsumed;
                        }
                }
            
            
        }
        break;
        // State SprawdzenieTemp
        case RecznaZmianaDown_SprawdzenieTemp:
        {
            if(IS_EVENT_TYPE_OF(evZadajTemp_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("14");
                    NOTIFY_STATE_EXITED("ROOT.RecznaZmianaDown.SprawdzenieTemp");
                    NOTIFY_STATE_ENTERED("ROOT.RecznaZmianaDown.Chlodzenie");
                    RecznaZmianaDown_subState = Chlodzenie;
                    rootState_active = Chlodzenie;
                    RecznaZmianaDown_timeout = scheduleTimeout(600, "ROOT.RecznaZmianaDown.Chlodzenie");
                    NOTIFY_TRANSITION_TERMINATED("14");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State Wylaczenie
        case RecznaZmianaDown_Wylaczenie:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == RecznaZmianaDown_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("26");
                            switch (RecznaZmianaDown_subState) {
                                // State Chlodzenie
                                case Chlodzenie:
                                {
                                    cancel(RecznaZmianaDown_timeout);
                                    NOTIFY_STATE_EXITED("ROOT.RecznaZmianaDown.Chlodzenie");
                                }
                                break;
                                // State SprawdzenieTemp
                                case RecznaZmianaDown_SprawdzenieTemp:
                                {
                                    NOTIFY_STATE_EXITED("ROOT.RecznaZmianaDown.SprawdzenieTemp");
                                }
                                break;
                                // State Wylaczenie
                                case RecznaZmianaDown_Wylaczenie:
                                {
                                    cancel(RecznaZmianaDown_timeout);
                                    NOTIFY_STATE_EXITED("ROOT.RecznaZmianaDown.Wylaczenie");
                                }
                                break;
                                default:
                                    break;
                            }
                            RecznaZmianaDown_subState = OMNonState;
                            NOTIFY_STATE_EXITED("ROOT.RecznaZmianaDown");
                            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                            rootState_subState = Oczekiwanie;
                            rootState_active = Oczekiwanie;
                            //#[ state Oczekiwanie.(Entry) 
                                          //cout<<endl<<"Oczekiwanie";
                                          //cout<<endl<<"Dostepne tryby:";  
                                          //cout<<endl<<"[W]yswietl rejestr";
                                          //cout<<endl<<"[A]utomatyczna zmiana temperatury";
                                          //cout<<endl<<"[G]Zwiekszenie temperatury";
                                          //cout<<endl<<"[C]Zmniejszenie temperatury";
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("26");
                            res = eventConsumed;
                        }
                }
            
            
        }
        break;
        // State Wlaczenie
        case AutoZmiana_Wlaczenie:
        {
            if(IS_EVENT_TYPE_OF(evZadajTemp_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("31");
                    NOTIFY_STATE_EXITED("ROOT.AutoZmiana.Wlaczenie");
                    NOTIFY_STATE_ENTERED("ROOT.AutoZmiana.Grzanie");
                    AutoZmiana_subState = AutoZmiana_Grzanie;
                    rootState_active = AutoZmiana_Grzanie;
                    AutoZmiana_timeout = scheduleTimeout(600, "ROOT.AutoZmiana.Grzanie");
                    NOTIFY_TRANSITION_TERMINATED("31");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State UstalCzas
        case UstalCzas:
        {
            if(IS_EVENT_TYPE_OF(evKeyTimeUp_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("7");
                    NOTIFY_STATE_EXITED("ROOT.AutoZmiana.UstalCzas");
                    NOTIFY_STATE_ENTERED("ROOT.AutoZmiana.SprawdzenieCzas");
                    AutoZmiana_subState = SprawdzenieCzas;
                    rootState_active = SprawdzenieCzas;
                    AutoZmiana_timeout = scheduleTimeout(60, "ROOT.AutoZmiana.SprawdzenieCzas");
                    NOTIFY_TRANSITION_TERMINATED("7");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(evKeyTimeDown_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("21");
                    NOTIFY_STATE_EXITED("ROOT.AutoZmiana.UstalCzas");
                    NOTIFY_STATE_ENTERED("ROOT.AutoZmiana.SprawdzenieCzas");
                    AutoZmiana_subState = SprawdzenieCzas;
                    rootState_active = SprawdzenieCzas;
                    AutoZmiana_timeout = scheduleTimeout(60, "ROOT.AutoZmiana.SprawdzenieCzas");
                    NOTIFY_TRANSITION_TERMINATED("21");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State SprawdzenieCzas
        case SprawdzenieCzas:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == AutoZmiana_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("5");
                            cancel(AutoZmiana_timeout);
                            NOTIFY_STATE_EXITED("ROOT.AutoZmiana.SprawdzenieCzas");
                            NOTIFY_STATE_ENTERED("ROOT.AutoZmiana.SprawdzenieCzas");
                            AutoZmiana_subState = SprawdzenieCzas;
                            rootState_active = SprawdzenieCzas;
                            AutoZmiana_timeout = scheduleTimeout(60, "ROOT.AutoZmiana.SprawdzenieCzas");
                            NOTIFY_TRANSITION_TERMINATED("5");
                            res = eventConsumed;
                        }
                }
            else if(IS_EVENT_TYPE_OF(evGetTemp_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("12");
                    cancel(AutoZmiana_timeout);
                    NOTIFY_STATE_EXITED("ROOT.AutoZmiana.SprawdzenieCzas");
                    NOTIFY_STATE_ENTERED("ROOT.AutoZmiana.SprawdzenieTemp");
                    AutoZmiana_subState = AutoZmiana_SprawdzenieTemp;
                    rootState_active = AutoZmiana_SprawdzenieTemp;
                    NOTIFY_TRANSITION_TERMINATED("12");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State SprawdzenieTemp
        case AutoZmiana_SprawdzenieTemp:
        {
            if(IS_EVENT_TYPE_OF(evZadajTemp_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("36");
                    NOTIFY_STATE_EXITED("ROOT.AutoZmiana.SprawdzenieTemp");
                    NOTIFY_STATE_ENTERED("ROOT.AutoZmiana.Chlodzenie");
                    AutoZmiana_subState = AutoZmiana_Chlodzenie;
                    rootState_active = AutoZmiana_Chlodzenie;
                    AutoZmiana_timeout = scheduleTimeout(600, "ROOT.AutoZmiana.Chlodzenie");
                    NOTIFY_TRANSITION_TERMINATED("36");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(evStart_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("15");
                    NOTIFY_STATE_EXITED("ROOT.AutoZmiana.SprawdzenieTemp");
                    NOTIFY_STATE_ENTERED("ROOT.AutoZmiana.Wlaczenie");
                    AutoZmiana_subState = AutoZmiana_Wlaczenie;
                    rootState_active = AutoZmiana_Wlaczenie;
                    NOTIFY_TRANSITION_TERMINATED("15");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State Chlodzenie
        case AutoZmiana_Chlodzenie:
        {
            if(IS_EVENT_TYPE_OF(evStop_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("28");
                    cancel(AutoZmiana_timeout);
                    NOTIFY_STATE_EXITED("ROOT.AutoZmiana.Chlodzenie");
                    NOTIFY_STATE_ENTERED("ROOT.AutoZmiana.Wylaczenie");
                    AutoZmiana_subState = AutoZmiana_Wylaczenie;
                    rootState_active = AutoZmiana_Wylaczenie;
                    AutoZmiana_timeout = scheduleTimeout(0, "ROOT.AutoZmiana.Wylaczenie");
                    NOTIFY_TRANSITION_TERMINATED("28");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == AutoZmiana_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("29");
                            cancel(AutoZmiana_timeout);
                            NOTIFY_STATE_EXITED("ROOT.AutoZmiana.Chlodzenie");
                            NOTIFY_STATE_ENTERED("ROOT.AutoZmiana.SprawdzenieTemp");
                            AutoZmiana_subState = AutoZmiana_SprawdzenieTemp;
                            rootState_active = AutoZmiana_SprawdzenieTemp;
                            NOTIFY_TRANSITION_TERMINATED("29");
                            res = eventConsumed;
                        }
                }
            
            
        }
        break;
        // State Wylaczenie
        case AutoZmiana_Wylaczenie:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == AutoZmiana_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("30");
                            AutoZmiana_exit();
                            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                            rootState_subState = Oczekiwanie;
                            rootState_active = Oczekiwanie;
                            //#[ state Oczekiwanie.(Entry) 
                                          //cout<<endl<<"Oczekiwanie";
                                          //cout<<endl<<"Dostepne tryby:";  
                                          //cout<<endl<<"[W]yswietl rejestr";
                                          //cout<<endl<<"[A]utomatyczna zmiana temperatury";
                                          //cout<<endl<<"[G]Zwiekszenie temperatury";
                                          //cout<<endl<<"[C]Zmniejszenie temperatury";
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("30");
                            res = eventConsumed;
                        }
                }
            
            
        }
        break;
        // State Grzanie
        case AutoZmiana_Grzanie:
        {
            if(IS_EVENT_TYPE_OF(evStop_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("32");
                    cancel(AutoZmiana_timeout);
                    NOTIFY_STATE_EXITED("ROOT.AutoZmiana.Grzanie");
                    NOTIFY_STATE_ENTERED("ROOT.AutoZmiana.WylaczenieG");
                    AutoZmiana_subState = WylaczenieG;
                    rootState_active = WylaczenieG;
                    AutoZmiana_timeout = scheduleTimeout(0, "ROOT.AutoZmiana.WylaczenieG");
                    NOTIFY_TRANSITION_TERMINATED("32");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == AutoZmiana_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("33");
                            cancel(AutoZmiana_timeout);
                            NOTIFY_STATE_EXITED("ROOT.AutoZmiana.Grzanie");
                            NOTIFY_STATE_ENTERED("ROOT.AutoZmiana.SprawdzenieTemp");
                            AutoZmiana_subState = AutoZmiana_SprawdzenieTemp;
                            rootState_active = AutoZmiana_SprawdzenieTemp;
                            NOTIFY_TRANSITION_TERMINATED("33");
                            res = eventConsumed;
                        }
                }
            
            
        }
        break;
        // State WylaczenieG
        case WylaczenieG:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == AutoZmiana_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("34");
                            AutoZmiana_exit();
                            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                            rootState_subState = Oczekiwanie;
                            rootState_active = Oczekiwanie;
                            //#[ state Oczekiwanie.(Entry) 
                                          //cout<<endl<<"Oczekiwanie";
                                          //cout<<endl<<"Dostepne tryby:";  
                                          //cout<<endl<<"[W]yswietl rejestr";
                                          //cout<<endl<<"[A]utomatyczna zmiana temperatury";
                                          //cout<<endl<<"[G]Zwiekszenie temperatury";
                                          //cout<<endl<<"[C]Zmniejszenie temperatury";
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("34");
                            res = eventConsumed;
                        }
                }
            
            
        }
        break;
        default:
            break;
    }
    return res;
}

void Regulator::RecznaZmianaUp_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.RecznaZmianaUp");
    rootState_subState = RecznaZmianaUp;
    NOTIFY_TRANSITION_STARTED("4");
    NOTIFY_STATE_ENTERED("ROOT.RecznaZmianaUp.SprawdzenieTemp");
    RecznaZmianaUp_subState = SprawdzenieTemp;
    rootState_active = SprawdzenieTemp;
    NOTIFY_TRANSITION_TERMINATED("4");
}

void Regulator::RecznaZmianaUp_exit() {
    switch (RecznaZmianaUp_subState) {
        // State SprawdzenieTemp
        case SprawdzenieTemp:
        {
            NOTIFY_STATE_EXITED("ROOT.RecznaZmianaUp.SprawdzenieTemp");
        }
        break;
        // State Wlaczenie
        case Wlaczenie:
        {
            NOTIFY_STATE_EXITED("ROOT.RecznaZmianaUp.Wlaczenie");
        }
        break;
        // State Grzanie
        case Grzanie:
        {
            cancel(RecznaZmianaUp_timeout);
            NOTIFY_STATE_EXITED("ROOT.RecznaZmianaUp.Grzanie");
        }
        break;
        // State Wylaczenie
        case Wylaczenie:
        {
            cancel(RecznaZmianaUp_timeout);
            NOTIFY_STATE_EXITED("ROOT.RecznaZmianaUp.Wylaczenie");
        }
        break;
        default:
            break;
    }
    RecznaZmianaUp_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.RecznaZmianaUp");
}

void Regulator::RecznaZmianaDown_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.RecznaZmianaDown");
    rootState_subState = RecznaZmianaDown;
    NOTIFY_TRANSITION_STARTED("18");
    NOTIFY_STATE_ENTERED("ROOT.RecznaZmianaDown.SprawdzenieTemp");
    RecznaZmianaDown_subState = RecznaZmianaDown_SprawdzenieTemp;
    rootState_active = RecznaZmianaDown_SprawdzenieTemp;
    NOTIFY_TRANSITION_TERMINATED("18");
}

IOxfReactive::TakeEventStatus Regulator::Oczekiwanie_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(evKeyDownPressed_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("20");
            //#[ state Oczekiwanie.(Exit) 
                          std::cin>>tryb;
            //#]
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            RecznaZmianaDown_entDef();
            NOTIFY_TRANSITION_TERMINATED("20");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evKeyUpPressed_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("2");
            //#[ state Oczekiwanie.(Exit) 
                          std::cin>>tryb;
            //#]
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            RecznaZmianaUp_entDef();
            NOTIFY_TRANSITION_TERMINATED("2");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evKeyRejestr_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("1");
            //#[ state Oczekiwanie.(Exit) 
                          std::cin>>tryb;
            //#]
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            NOTIFY_STATE_ENTERED("ROOT.WyswietlenieRejestru");
            rootState_subState = WyswietlenieRejestru;
            rootState_active = WyswietlenieRejestru;
            //#[ state WyswietlenieRejestru.(Entry) 
                                   std::cout<<"Wyswietlenie rejestru";
            //#]
            NOTIFY_TRANSITION_TERMINATED("1");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evAutoTempKey_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("3");
            //#[ state Oczekiwanie.(Exit) 
                          std::cin>>tryb;
            //#]
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            AutoZmiana_entDef();
            NOTIFY_TRANSITION_TERMINATED("3");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evAwaria_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("16");
            //#[ state Oczekiwanie.(Exit) 
                          std::cin>>tryb;
            //#]
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            NOTIFY_STATE_ENTERED("ROOT.Awaria");
            pushNullTransition();
            rootState_subState = Awaria;
            rootState_active = Awaria;
            NOTIFY_TRANSITION_TERMINATED("16");
            res = eventConsumed;
        }
    
    return res;
}

void Regulator::AutoZmiana_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.AutoZmiana");
    rootState_subState = AutoZmiana;
    NOTIFY_TRANSITION_STARTED("6");
    NOTIFY_STATE_ENTERED("ROOT.AutoZmiana.UstalCzas");
    AutoZmiana_subState = UstalCzas;
    rootState_active = UstalCzas;
    //#[ state AutoZmiana.UstalCzas.(Entry) 
                //cout<<endl<<"Wprowadz czas zmiany";
                //cout<<endl<<"[0]Zwieksz czas";
                //cout<<endl<<"[1]Zmniejsz czas";
    //#]
    NOTIFY_TRANSITION_TERMINATED("6");
}

void Regulator::AutoZmiana_exit() {
    switch (AutoZmiana_subState) {
        // State Wlaczenie
        case AutoZmiana_Wlaczenie:
        {
            NOTIFY_STATE_EXITED("ROOT.AutoZmiana.Wlaczenie");
        }
        break;
        // State UstalCzas
        case UstalCzas:
        {
            NOTIFY_STATE_EXITED("ROOT.AutoZmiana.UstalCzas");
        }
        break;
        // State SprawdzenieCzas
        case SprawdzenieCzas:
        {
            cancel(AutoZmiana_timeout);
            NOTIFY_STATE_EXITED("ROOT.AutoZmiana.SprawdzenieCzas");
        }
        break;
        // State SprawdzenieTemp
        case AutoZmiana_SprawdzenieTemp:
        {
            NOTIFY_STATE_EXITED("ROOT.AutoZmiana.SprawdzenieTemp");
        }
        break;
        // State Chlodzenie
        case AutoZmiana_Chlodzenie:
        {
            cancel(AutoZmiana_timeout);
            NOTIFY_STATE_EXITED("ROOT.AutoZmiana.Chlodzenie");
        }
        break;
        // State Wylaczenie
        case AutoZmiana_Wylaczenie:
        {
            cancel(AutoZmiana_timeout);
            NOTIFY_STATE_EXITED("ROOT.AutoZmiana.Wylaczenie");
        }
        break;
        // State Grzanie
        case AutoZmiana_Grzanie:
        {
            cancel(AutoZmiana_timeout);
            NOTIFY_STATE_EXITED("ROOT.AutoZmiana.Grzanie");
        }
        break;
        // State WylaczenieG
        case WylaczenieG:
        {
            cancel(AutoZmiana_timeout);
            NOTIFY_STATE_EXITED("ROOT.AutoZmiana.WylaczenieG");
        }
        break;
        default:
            break;
    }
    AutoZmiana_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.AutoZmiana");
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedRegulator::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("tryb", x2String(myReal->tryb));
}

void OMAnimatedRegulator::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsSterownik);
}

void OMAnimatedRegulator::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Regulator::Oczekiwanie:
        {
            Oczekiwanie_serializeStates(aomsState);
        }
        break;
        case Regulator::WyswietlenieRejestru:
        {
            WyswietlenieRejestru_serializeStates(aomsState);
        }
        break;
        case Regulator::Awaria:
        {
            Awaria_serializeStates(aomsState);
        }
        break;
        case Regulator::terminationstate_30:
        {
            terminationstate_30_serializeStates(aomsState);
        }
        break;
        case Regulator::RecznaZmianaUp:
        {
            RecznaZmianaUp_serializeStates(aomsState);
        }
        break;
        case Regulator::RecznaZmianaDown:
        {
            RecznaZmianaDown_serializeStates(aomsState);
        }
        break;
        case Regulator::AutoZmiana:
        {
            AutoZmiana_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedRegulator::WyswietlenieRejestru_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.WyswietlenieRejestru");
}

void OMAnimatedRegulator::terminationstate_30_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.terminationstate_30");
}

void OMAnimatedRegulator::RecznaZmianaUp_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.RecznaZmianaUp");
    switch (myReal->RecznaZmianaUp_subState) {
        case Regulator::SprawdzenieTemp:
        {
            SprawdzenieTemp_serializeStates(aomsState);
        }
        break;
        case Regulator::Wlaczenie:
        {
            Wlaczenie_serializeStates(aomsState);
        }
        break;
        case Regulator::Grzanie:
        {
            Grzanie_serializeStates(aomsState);
        }
        break;
        case Regulator::Wylaczenie:
        {
            Wylaczenie_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedRegulator::Wylaczenie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.RecznaZmianaUp.Wylaczenie");
}

void OMAnimatedRegulator::Wlaczenie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.RecznaZmianaUp.Wlaczenie");
}

void OMAnimatedRegulator::SprawdzenieTemp_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.RecznaZmianaUp.SprawdzenieTemp");
}

void OMAnimatedRegulator::Grzanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.RecznaZmianaUp.Grzanie");
}

void OMAnimatedRegulator::RecznaZmianaDown_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.RecznaZmianaDown");
    switch (myReal->RecznaZmianaDown_subState) {
        case Regulator::Chlodzenie:
        {
            Chlodzenie_serializeStates(aomsState);
        }
        break;
        case Regulator::RecznaZmianaDown_SprawdzenieTemp:
        {
            RecznaZmianaDown_SprawdzenieTemp_serializeStates(aomsState);
        }
        break;
        case Regulator::RecznaZmianaDown_Wylaczenie:
        {
            RecznaZmianaDown_Wylaczenie_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedRegulator::RecznaZmianaDown_Wylaczenie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.RecznaZmianaDown.Wylaczenie");
}

void OMAnimatedRegulator::RecznaZmianaDown_SprawdzenieTemp_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.RecznaZmianaDown.SprawdzenieTemp");
}

void OMAnimatedRegulator::Chlodzenie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.RecznaZmianaDown.Chlodzenie");
}

void OMAnimatedRegulator::Oczekiwanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Oczekiwanie");
}

void OMAnimatedRegulator::Awaria_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Awaria");
}

void OMAnimatedRegulator::AutoZmiana_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.AutoZmiana");
    switch (myReal->AutoZmiana_subState) {
        case Regulator::AutoZmiana_Wlaczenie:
        {
            AutoZmiana_Wlaczenie_serializeStates(aomsState);
        }
        break;
        case Regulator::UstalCzas:
        {
            UstalCzas_serializeStates(aomsState);
        }
        break;
        case Regulator::SprawdzenieCzas:
        {
            SprawdzenieCzas_serializeStates(aomsState);
        }
        break;
        case Regulator::AutoZmiana_SprawdzenieTemp:
        {
            AutoZmiana_SprawdzenieTemp_serializeStates(aomsState);
        }
        break;
        case Regulator::AutoZmiana_Chlodzenie:
        {
            AutoZmiana_Chlodzenie_serializeStates(aomsState);
        }
        break;
        case Regulator::AutoZmiana_Wylaczenie:
        {
            AutoZmiana_Wylaczenie_serializeStates(aomsState);
        }
        break;
        case Regulator::AutoZmiana_Grzanie:
        {
            AutoZmiana_Grzanie_serializeStates(aomsState);
        }
        break;
        case Regulator::WylaczenieG:
        {
            WylaczenieG_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedRegulator::WylaczenieG_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.AutoZmiana.WylaczenieG");
}

void OMAnimatedRegulator::AutoZmiana_Wylaczenie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.AutoZmiana.Wylaczenie");
}

void OMAnimatedRegulator::AutoZmiana_Wlaczenie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.AutoZmiana.Wlaczenie");
}

void OMAnimatedRegulator::UstalCzas_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.AutoZmiana.UstalCzas");
}

void OMAnimatedRegulator::AutoZmiana_SprawdzenieTemp_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.AutoZmiana.SprawdzenieTemp");
}

void OMAnimatedRegulator::SprawdzenieCzas_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.AutoZmiana.SprawdzenieCzas");
}

void OMAnimatedRegulator::AutoZmiana_Grzanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.AutoZmiana.Grzanie");
}

void OMAnimatedRegulator::AutoZmiana_Chlodzenie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.AutoZmiana.Chlodzenie");
}
//#]

IMPLEMENT_REACTIVE_META_P(Regulator, Default, Default, false, OMAnimatedRegulator)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Regulator.cpp
*********************************************************************/
