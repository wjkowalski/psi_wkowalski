/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Czujnik
//!	Generated Date	: Tue, 10, Sep 2019  
	File Path	: DefaultComponent/DefaultConfig/Czujnik.h
*********************************************************************/

#ifndef Czujnik_H
#define Czujnik_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Czujnik
#include "Modul.h"
//## link itsSterownik
class Sterownik;

//## package Default

//## class Czujnik
class Czujnik : public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedCzujnik;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Czujnik(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    virtual ~Czujnik();
    
    ////    Operations    ////
    
    //## operation getOption()
    virtual RhpString getOption();
    
    //## operation off()
    virtual bool off();
    
    //## operation on()
    virtual bool on();
    
    //## operation setOption(RhpString)
    virtual bool setOption(const RhpString& option);
    
    ////    Additional operations    ////
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    void setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void cleanUpRelations();

private :

    //## auto_generated
    Stan getStanC() const;
    
    //## auto_generated
    void setStanC(Stan p_StanC);
    
    //## auto_generated
    int getTempBiez() const;
    
    //## auto_generated
    void setTempBiez(int p_TempBiez);
    
    ////    Attributes    ////

protected :

    Stan StanC;		//## attribute StanC
    
    int TempBiez;		//## attribute TempBiez
    
    ////    Relations and components    ////
    
    Sterownik* itsSterownik;		//## link itsSterownik
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _clearItsSterownik();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedCzujnik : public OMAnimatedModul {
    DECLARE_META(Czujnik, OMAnimatedCzujnik)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Czujnik.h
*********************************************************************/
