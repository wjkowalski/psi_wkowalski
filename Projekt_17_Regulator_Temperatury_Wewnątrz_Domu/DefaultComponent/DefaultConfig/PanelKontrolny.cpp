/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: PanelKontrolny
//!	Generated Date	: Tue, 10, Sep 2019  
	File Path	: DefaultComponent/DefaultConfig/PanelKontrolny.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "PanelKontrolny.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define Default_PanelKontrolny_PanelKontrolny_SERIALIZE OM_NO_OP

#define Default_PanelKontrolny_getOption_SERIALIZE OM_NO_OP

#define Default_PanelKontrolny_off_SERIALIZE OM_NO_OP

#define Default_PanelKontrolny_on_SERIALIZE OM_NO_OP

#define Default_PanelKontrolny_setOption_SERIALIZE aomsmethod->addAttribute("option", x2String(option));
//#]

//## package Default

//## class PanelKontrolny
PanelKontrolny::PanelKontrolny(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(PanelKontrolny, PanelKontrolny(), 0, Default_PanelKontrolny_PanelKontrolny_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsSterownik = NULL;
    initStatechart();
}

PanelKontrolny::~PanelKontrolny() {
    NOTIFY_DESTRUCTOR(~PanelKontrolny, false);
    cleanUpRelations();
    cancelTimeouts();
}

RhpString PanelKontrolny::getOption() {
    NOTIFY_OPERATION(getOption, getOption(), 0, Default_PanelKontrolny_getOption_SERIALIZE);
    //#[ operation getOption()
    return "x";
    //#]
}

bool PanelKontrolny::off() {
    NOTIFY_OPERATION(off, off(), 0, Default_PanelKontrolny_off_SERIALIZE);
    //#[ operation off()
    return true;
    //#]
}

bool PanelKontrolny::on() {
    NOTIFY_OPERATION(on, on(), 0, Default_PanelKontrolny_on_SERIALIZE);
    //#[ operation on()
    return true;
    //#]
}

bool PanelKontrolny::setOption(const RhpString& option) {
    NOTIFY_OPERATION(setOption, setOption(const RhpString&), 1, Default_PanelKontrolny_setOption_SERIALIZE);
    //#[ operation setOption(RhpString)
     return true;
    //#]
}

Sterownik* PanelKontrolny::getItsSterownik() const {
    return itsSterownik;
}

void PanelKontrolny::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool PanelKontrolny::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void PanelKontrolny::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
    rootState_timeout = NULL;
}

void PanelKontrolny::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

void PanelKontrolny::cancelTimeouts() {
    cancel(rootState_timeout);
}

bool PanelKontrolny::cancelTimeout(const IOxfTimeout* arg) {
    bool res = false;
    if(rootState_timeout == arg)
        {
            rootState_timeout = NULL;
            res = true;
        }
    return res;
}

void PanelKontrolny::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void PanelKontrolny::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void PanelKontrolny::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

void PanelKontrolny::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
        rootState_subState = Oczekiwanie;
        rootState_active = Oczekiwanie;
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus PanelKontrolny::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Oczekiwanie
        case Oczekiwanie:
        {
            res = Oczekiwanie_handleEvent();
        }
        break;
        // State sendaction_1
        case sendaction_1:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == rootState_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("8");
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.sendaction_1");
                            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                            rootState_subState = Oczekiwanie;
                            rootState_active = Oczekiwanie;
                            NOTIFY_TRANSITION_TERMINATED("8");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State sendaction_2
        case sendaction_2:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == rootState_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("9");
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.sendaction_2");
                            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                            rootState_subState = Oczekiwanie;
                            rootState_active = Oczekiwanie;
                            NOTIFY_TRANSITION_TERMINATED("9");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State sendaction_3
        case sendaction_3:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == rootState_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("10");
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.sendaction_3");
                            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                            rootState_subState = Oczekiwanie;
                            rootState_active = Oczekiwanie;
                            NOTIFY_TRANSITION_TERMINATED("10");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State sendaction_4
        case sendaction_4:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == rootState_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("12");
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.sendaction_4");
                            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                            rootState_subState = Oczekiwanie;
                            rootState_active = Oczekiwanie;
                            NOTIFY_TRANSITION_TERMINATED("12");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State sendaction_5
        case sendaction_5:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == rootState_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("11");
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.sendaction_5");
                            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                            rootState_subState = Oczekiwanie;
                            rootState_active = Oczekiwanie;
                            NOTIFY_TRANSITION_TERMINATED("11");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State sendaction_6
        case sendaction_6:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == rootState_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("7");
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.sendaction_6");
                            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                            rootState_subState = Oczekiwanie;
                            rootState_active = Oczekiwanie;
                            NOTIFY_TRANSITION_TERMINATED("7");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

IOxfReactive::TakeEventStatus PanelKontrolny::Oczekiwanie_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(evKeyDownPressed_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("6");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            NOTIFY_STATE_ENTERED("ROOT.sendaction_5");
            rootState_subState = sendaction_5;
            rootState_active = sendaction_5;
            //#[ state sendaction_5.(Entry) 
            itsSterownik->GEN(evKeyDownPressed);
            //#]
            rootState_timeout = scheduleTimeout(0, "ROOT.sendaction_5");
            NOTIFY_TRANSITION_TERMINATED("6");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evKeyTimeUp_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("3");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            NOTIFY_STATE_ENTERED("ROOT.sendaction_2");
            rootState_subState = sendaction_2;
            rootState_active = sendaction_2;
            //#[ state sendaction_2.(Entry) 
            itsSterownik->GEN(evKeyTimeUp);
            //#]
            rootState_timeout = scheduleTimeout(0, "ROOT.sendaction_2");
            NOTIFY_TRANSITION_TERMINATED("3");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evKeyUpPressed_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("2");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            NOTIFY_STATE_ENTERED("ROOT.sendaction_1");
            rootState_subState = sendaction_1;
            rootState_active = sendaction_1;
            //#[ state sendaction_1.(Entry) 
            itsSterownik->GEN(evKeyUpPressed);
            //#]
            rootState_timeout = scheduleTimeout(0, "ROOT.sendaction_1");
            NOTIFY_TRANSITION_TERMINATED("2");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evKeyRejestr_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("1");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            NOTIFY_STATE_ENTERED("ROOT.sendaction_6");
            rootState_subState = sendaction_6;
            rootState_active = sendaction_6;
            //#[ state sendaction_6.(Entry) 
            itsSterownik->GEN(evKeyRejestr);
            //#]
            rootState_timeout = scheduleTimeout(0, "ROOT.sendaction_6");
            NOTIFY_TRANSITION_TERMINATED("1");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evAutoTempKey_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("4");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            NOTIFY_STATE_ENTERED("ROOT.sendaction_3");
            rootState_subState = sendaction_3;
            rootState_active = sendaction_3;
            //#[ state sendaction_3.(Entry) 
            itsSterownik->GEN(evAutoTempKey);
            //#]
            rootState_timeout = scheduleTimeout(0, "ROOT.sendaction_3");
            NOTIFY_TRANSITION_TERMINATED("4");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evKeyTimeDown_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("5");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            NOTIFY_STATE_ENTERED("ROOT.sendaction_4");
            rootState_subState = sendaction_4;
            rootState_active = sendaction_4;
            //#[ state sendaction_4.(Entry) 
            itsSterownik->GEN(evKeyTimeDown);
            //#]
            rootState_timeout = scheduleTimeout(0, "ROOT.sendaction_4");
            NOTIFY_TRANSITION_TERMINATED("5");
            res = eventConsumed;
        }
    
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedPanelKontrolny::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedPanelKontrolny::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}

void OMAnimatedPanelKontrolny::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case PanelKontrolny::Oczekiwanie:
        {
            Oczekiwanie_serializeStates(aomsState);
        }
        break;
        case PanelKontrolny::sendaction_1:
        {
            sendaction_1_serializeStates(aomsState);
        }
        break;
        case PanelKontrolny::sendaction_2:
        {
            sendaction_2_serializeStates(aomsState);
        }
        break;
        case PanelKontrolny::sendaction_3:
        {
            sendaction_3_serializeStates(aomsState);
        }
        break;
        case PanelKontrolny::sendaction_4:
        {
            sendaction_4_serializeStates(aomsState);
        }
        break;
        case PanelKontrolny::sendaction_5:
        {
            sendaction_5_serializeStates(aomsState);
        }
        break;
        case PanelKontrolny::sendaction_6:
        {
            sendaction_6_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedPanelKontrolny::sendaction_6_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_6");
}

void OMAnimatedPanelKontrolny::sendaction_5_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_5");
}

void OMAnimatedPanelKontrolny::sendaction_4_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_4");
}

void OMAnimatedPanelKontrolny::sendaction_3_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_3");
}

void OMAnimatedPanelKontrolny::sendaction_2_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_2");
}

void OMAnimatedPanelKontrolny::sendaction_1_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_1");
}

void OMAnimatedPanelKontrolny::Oczekiwanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Oczekiwanie");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(PanelKontrolny, Default, false, Modul, OMAnimatedModul, OMAnimatedPanelKontrolny)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/PanelKontrolny.cpp
*********************************************************************/
