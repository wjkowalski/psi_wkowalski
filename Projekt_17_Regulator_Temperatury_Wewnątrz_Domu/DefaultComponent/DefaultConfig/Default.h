/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Default
//!	Generated Date	: Tue, 10, Sep 2019  
	File Path	: DefaultComponent/DefaultConfig/Default.h
*********************************************************************/

#ifndef Default_H
#define Default_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include <oxf/event.h>
//## auto_generated
class Czujnik;

//## auto_generated
class Grzejnik;

//## auto_generated
class Modul;

//## auto_generated
class PanelKontrolny;

//## auto_generated
class Regulator;

//## auto_generated
class Rejestr;

//## auto_generated
class Sterownik;

//#[ ignore
#define evZmiana_Default_id 18601

#define evRedukujG_Default_id 18602

#define evStop_Default_id 18603

#define evZadajTemp_Default_id 18604

#define evStart_Default_id 18605

#define evZwiekszG_Default_id 18606

#define evSprawdzStan_Default_id 18607

#define evGetTemp_Default_id 18608

#define evCzujnikKaput_Default_id 18609

#define evResetujStan_Default_id 18610

#define evZmienCzas_Default_id 18611

#define evZwiekszTemp_Default_id 18612

#define evZmniejszTemp_Default_id 18613

#define evGetTime_Default_id 18614

#define evKeyUpPressed_Default_id 18615

#define evAutoTempKey_Default_id 18616

#define evKeyTimeUp_Default_id 18617

#define evKeyRejestr_Default_id 18618

#define evAskRejestr_Default_id 18619

#define evSaveTime_Default_id 18620

#define evAutoKeyPressed_Default_id 18621

#define evAwaria_Default_id 18622

#define evKeyDownPressed_Default_id 18623

#define evKeyTimeDown_Default_id 18624

#define evReplyRejestr_Default_id 18625

#define evKeyUp_Default_id 18626
//#]

//## package Default


//## type Operacja
enum Operacja {
    ZmianaUp = 0,
    ZmianaDown = 1,
    StanObecny = 2,
    WyswietlRejestr = 3
};

//## type Stan
enum Stan {
    OFF = 0,
    ON = 1
};

//## type AutomatZmiana
enum AutomatZmiana {
    AutoZmianaUp = 0,
    AutoZmianaDown = 1,
    BrakAutomat = 2
};

//## event evZmiana()
class evZmiana : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevZmiana;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evZmiana();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevZmiana : virtual public AOMEvent {
    DECLARE_META_EVENT(evZmiana)
};
//#]
#endif // _OMINSTRUMENT

//## event evRedukujG()
class evRedukujG : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevRedukujG;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evRedukujG();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevRedukujG : virtual public AOMEvent {
    DECLARE_META_EVENT(evRedukujG)
};
//#]
#endif // _OMINSTRUMENT

//## event evStop()
class evStop : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevStop;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evStop();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevStop : virtual public AOMEvent {
    DECLARE_META_EVENT(evStop)
};
//#]
#endif // _OMINSTRUMENT

//## event evZadajTemp()
class evZadajTemp : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevZadajTemp;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evZadajTemp();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevZadajTemp : virtual public AOMEvent {
    DECLARE_META_EVENT(evZadajTemp)
};
//#]
#endif // _OMINSTRUMENT

//## event evStart()
class evStart : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevStart;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evStart();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevStart : virtual public AOMEvent {
    DECLARE_META_EVENT(evStart)
};
//#]
#endif // _OMINSTRUMENT

//## event evZwiekszG()
class evZwiekszG : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevZwiekszG;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evZwiekszG();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevZwiekszG : virtual public AOMEvent {
    DECLARE_META_EVENT(evZwiekszG)
};
//#]
#endif // _OMINSTRUMENT

//## event evSprawdzStan()
class evSprawdzStan : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevSprawdzStan;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evSprawdzStan();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevSprawdzStan : virtual public AOMEvent {
    DECLARE_META_EVENT(evSprawdzStan)
};
//#]
#endif // _OMINSTRUMENT

//## event evGetTemp()
class evGetTemp : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevGetTemp;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evGetTemp();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevGetTemp : virtual public AOMEvent {
    DECLARE_META_EVENT(evGetTemp)
};
//#]
#endif // _OMINSTRUMENT

//## event evCzujnikKaput()
class evCzujnikKaput : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevCzujnikKaput;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evCzujnikKaput();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevCzujnikKaput : virtual public AOMEvent {
    DECLARE_META_EVENT(evCzujnikKaput)
};
//#]
#endif // _OMINSTRUMENT

//## event evResetujStan()
class evResetujStan : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevResetujStan;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evResetujStan();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevResetujStan : virtual public AOMEvent {
    DECLARE_META_EVENT(evResetujStan)
};
//#]
#endif // _OMINSTRUMENT

//## event evZmienCzas()
class evZmienCzas : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevZmienCzas;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evZmienCzas();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevZmienCzas : virtual public AOMEvent {
    DECLARE_META_EVENT(evZmienCzas)
};
//#]
#endif // _OMINSTRUMENT

//## event evZwiekszTemp()
class evZwiekszTemp : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevZwiekszTemp;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evZwiekszTemp();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevZwiekszTemp : virtual public AOMEvent {
    DECLARE_META_EVENT(evZwiekszTemp)
};
//#]
#endif // _OMINSTRUMENT

//## event evZmniejszTemp()
class evZmniejszTemp : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevZmniejszTemp;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evZmniejszTemp();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevZmniejszTemp : virtual public AOMEvent {
    DECLARE_META_EVENT(evZmniejszTemp)
};
//#]
#endif // _OMINSTRUMENT

//## event evGetTime()
class evGetTime : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevGetTime;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evGetTime();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevGetTime : virtual public AOMEvent {
    DECLARE_META_EVENT(evGetTime)
};
//#]
#endif // _OMINSTRUMENT

//## event evKeyUpPressed()
class evKeyUpPressed : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevKeyUpPressed;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evKeyUpPressed();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevKeyUpPressed : virtual public AOMEvent {
    DECLARE_META_EVENT(evKeyUpPressed)
};
//#]
#endif // _OMINSTRUMENT

//## event evAutoTempKey()
class evAutoTempKey : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevAutoTempKey;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evAutoTempKey();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevAutoTempKey : virtual public AOMEvent {
    DECLARE_META_EVENT(evAutoTempKey)
};
//#]
#endif // _OMINSTRUMENT

//## event evKeyTimeUp()
class evKeyTimeUp : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevKeyTimeUp;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evKeyTimeUp();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevKeyTimeUp : virtual public AOMEvent {
    DECLARE_META_EVENT(evKeyTimeUp)
};
//#]
#endif // _OMINSTRUMENT

//## event evKeyRejestr()
class evKeyRejestr : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevKeyRejestr;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evKeyRejestr();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevKeyRejestr : virtual public AOMEvent {
    DECLARE_META_EVENT(evKeyRejestr)
};
//#]
#endif // _OMINSTRUMENT

//## event evAskRejestr()
class evAskRejestr : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevAskRejestr;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evAskRejestr();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevAskRejestr : virtual public AOMEvent {
    DECLARE_META_EVENT(evAskRejestr)
};
//#]
#endif // _OMINSTRUMENT

//## event evSaveTime()
class evSaveTime : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevSaveTime;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evSaveTime();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevSaveTime : virtual public AOMEvent {
    DECLARE_META_EVENT(evSaveTime)
};
//#]
#endif // _OMINSTRUMENT

//## event evAutoKeyPressed()
class evAutoKeyPressed : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevAutoKeyPressed;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evAutoKeyPressed();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevAutoKeyPressed : virtual public AOMEvent {
    DECLARE_META_EVENT(evAutoKeyPressed)
};
//#]
#endif // _OMINSTRUMENT

//## event evAwaria()
class evAwaria : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevAwaria;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evAwaria();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevAwaria : virtual public AOMEvent {
    DECLARE_META_EVENT(evAwaria)
};
//#]
#endif // _OMINSTRUMENT

//## event evKeyDownPressed()
class evKeyDownPressed : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevKeyDownPressed;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evKeyDownPressed();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevKeyDownPressed : virtual public AOMEvent {
    DECLARE_META_EVENT(evKeyDownPressed)
};
//#]
#endif // _OMINSTRUMENT

//## event evKeyTimeDown()
class evKeyTimeDown : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevKeyTimeDown;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evKeyTimeDown();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevKeyTimeDown : virtual public AOMEvent {
    DECLARE_META_EVENT(evKeyTimeDown)
};
//#]
#endif // _OMINSTRUMENT

//## event evReplyRejestr()
class evReplyRejestr : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevReplyRejestr;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evReplyRejestr();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevReplyRejestr : virtual public AOMEvent {
    DECLARE_META_EVENT(evReplyRejestr)
};
//#]
#endif // _OMINSTRUMENT

//## event evKeyUp()
class evKeyUp : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevKeyUp;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evKeyUp();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevKeyUp : virtual public AOMEvent {
    DECLARE_META_EVENT(evKeyUp)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default.h
*********************************************************************/
