/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Regulator
//!	Generated Date	: Tue, 10, Sep 2019  
	File Path	: DefaultComponent/DefaultConfig/Regulator.h
*********************************************************************/

#ifndef Regulator_H
#define Regulator_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## link itsSterownik
#include "Sterownik.h"
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include <oxf/omthread.h>
//## package Default

//## class Regulator
class Regulator : public OMReactive {
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedRegulator;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Regulator(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Regulator();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cancelTimeouts();
    
    //## auto_generated
    bool cancelTimeout(const IOxfTimeout* arg);

private :

    //## auto_generated
    char getTryb() const;
    
    //## auto_generated
    void setTryb(char p_tryb);
    
    ////    Attributes    ////

protected :

    char tryb;		//## attribute tryb
    
    ////    Relations and components    ////
    
    Sterownik itsSterownik;		//## link itsSterownik
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void setActiveContext(IOxfActive* theActiveContext, bool activeInstance);
    
    //## auto_generated
    virtual void destroy();
    
    ////    Framework    ////
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    inline bool rootState_isCompleted();
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // WyswietlenieRejestru:
    //## statechart_method
    inline bool WyswietlenieRejestru_IN() const;
    
    // terminationstate_30:
    //## statechart_method
    inline bool terminationstate_30_IN() const;
    
    // RecznaZmianaUp:
    //## statechart_method
    inline bool RecznaZmianaUp_IN() const;
    
    //## statechart_method
    void RecznaZmianaUp_entDef();
    
    //## statechart_method
    void RecznaZmianaUp_exit();
    
    // Wylaczenie:
    //## statechart_method
    inline bool Wylaczenie_IN() const;
    
    // Wlaczenie:
    //## statechart_method
    inline bool Wlaczenie_IN() const;
    
    // SprawdzenieTemp:
    //## statechart_method
    inline bool SprawdzenieTemp_IN() const;
    
    // Grzanie:
    //## statechart_method
    inline bool Grzanie_IN() const;
    
    // RecznaZmianaDown:
    //## statechart_method
    inline bool RecznaZmianaDown_IN() const;
    
    //## statechart_method
    void RecznaZmianaDown_entDef();
    
    // RecznaZmianaDown_Wylaczenie:
    //## statechart_method
    inline bool RecznaZmianaDown_Wylaczenie_IN() const;
    
    // RecznaZmianaDown_SprawdzenieTemp:
    //## statechart_method
    inline bool RecznaZmianaDown_SprawdzenieTemp_IN() const;
    
    // Chlodzenie:
    //## statechart_method
    inline bool Chlodzenie_IN() const;
    
    // Oczekiwanie:
    //## statechart_method
    inline bool Oczekiwanie_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Oczekiwanie_handleEvent();
    
    // Awaria:
    //## statechart_method
    inline bool Awaria_IN() const;
    
    // AutoZmiana:
    //## statechart_method
    inline bool AutoZmiana_IN() const;
    
    //## statechart_method
    void AutoZmiana_entDef();
    
    //## statechart_method
    void AutoZmiana_exit();
    
    // WylaczenieG:
    //## statechart_method
    inline bool WylaczenieG_IN() const;
    
    // AutoZmiana_Wylaczenie:
    //## statechart_method
    inline bool AutoZmiana_Wylaczenie_IN() const;
    
    // AutoZmiana_Wlaczenie:
    //## statechart_method
    inline bool AutoZmiana_Wlaczenie_IN() const;
    
    // UstalCzas:
    //## statechart_method
    inline bool UstalCzas_IN() const;
    
    // AutoZmiana_SprawdzenieTemp:
    //## statechart_method
    inline bool AutoZmiana_SprawdzenieTemp_IN() const;
    
    // SprawdzenieCzas:
    //## statechart_method
    inline bool SprawdzenieCzas_IN() const;
    
    // AutoZmiana_Grzanie:
    //## statechart_method
    inline bool AutoZmiana_Grzanie_IN() const;
    
    // AutoZmiana_Chlodzenie:
    //## statechart_method
    inline bool AutoZmiana_Chlodzenie_IN() const;

protected :

//#[ ignore
    enum Regulator_Enum {
        OMNonState = 0,
        WyswietlenieRejestru = 1,
        terminationstate_30 = 2,
        RecznaZmianaUp = 3,
        Wylaczenie = 4,
        Wlaczenie = 5,
        SprawdzenieTemp = 6,
        Grzanie = 7,
        RecznaZmianaDown = 8,
        RecznaZmianaDown_Wylaczenie = 9,
        RecznaZmianaDown_SprawdzenieTemp = 10,
        Chlodzenie = 11,
        Oczekiwanie = 12,
        Awaria = 13,
        AutoZmiana = 14,
        WylaczenieG = 15,
        AutoZmiana_Wylaczenie = 16,
        AutoZmiana_Wlaczenie = 17,
        UstalCzas = 18,
        AutoZmiana_SprawdzenieTemp = 19,
        SprawdzenieCzas = 20,
        AutoZmiana_Grzanie = 21,
        AutoZmiana_Chlodzenie = 22
    };
    
    int rootState_subState;
    
    int rootState_active;
    
    int RecznaZmianaUp_subState;
    
    IOxfTimeout* RecznaZmianaUp_timeout;
    
    int RecznaZmianaDown_subState;
    
    IOxfTimeout* RecznaZmianaDown_timeout;
    
    int AutoZmiana_subState;
    
    IOxfTimeout* AutoZmiana_timeout;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedRegulator : virtual public AOMInstance {
    DECLARE_REACTIVE_META(Regulator, OMAnimatedRegulator)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void WyswietlenieRejestru_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void terminationstate_30_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void RecznaZmianaUp_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wylaczenie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wlaczenie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void SprawdzenieTemp_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Grzanie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void RecznaZmianaDown_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void RecznaZmianaDown_Wylaczenie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void RecznaZmianaDown_SprawdzenieTemp_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Chlodzenie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Oczekiwanie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Awaria_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void AutoZmiana_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void WylaczenieG_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void AutoZmiana_Wylaczenie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void AutoZmiana_Wlaczenie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void UstalCzas_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void AutoZmiana_SprawdzenieTemp_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void SprawdzenieCzas_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void AutoZmiana_Grzanie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void AutoZmiana_Chlodzenie_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Regulator::rootState_IN() const {
    return true;
}

inline bool Regulator::rootState_isCompleted() {
    return ( IS_IN(terminationstate_30) );
}

inline bool Regulator::WyswietlenieRejestru_IN() const {
    return rootState_subState == WyswietlenieRejestru;
}

inline bool Regulator::terminationstate_30_IN() const {
    return rootState_subState == terminationstate_30;
}

inline bool Regulator::RecznaZmianaUp_IN() const {
    return rootState_subState == RecznaZmianaUp;
}

inline bool Regulator::Wylaczenie_IN() const {
    return RecznaZmianaUp_subState == Wylaczenie;
}

inline bool Regulator::Wlaczenie_IN() const {
    return RecznaZmianaUp_subState == Wlaczenie;
}

inline bool Regulator::SprawdzenieTemp_IN() const {
    return RecznaZmianaUp_subState == SprawdzenieTemp;
}

inline bool Regulator::Grzanie_IN() const {
    return RecznaZmianaUp_subState == Grzanie;
}

inline bool Regulator::RecznaZmianaDown_IN() const {
    return rootState_subState == RecznaZmianaDown;
}

inline bool Regulator::RecznaZmianaDown_Wylaczenie_IN() const {
    return RecznaZmianaDown_subState == RecznaZmianaDown_Wylaczenie;
}

inline bool Regulator::RecznaZmianaDown_SprawdzenieTemp_IN() const {
    return RecznaZmianaDown_subState == RecznaZmianaDown_SprawdzenieTemp;
}

inline bool Regulator::Chlodzenie_IN() const {
    return RecznaZmianaDown_subState == Chlodzenie;
}

inline bool Regulator::Oczekiwanie_IN() const {
    return rootState_subState == Oczekiwanie;
}

inline bool Regulator::Awaria_IN() const {
    return rootState_subState == Awaria;
}

inline bool Regulator::AutoZmiana_IN() const {
    return rootState_subState == AutoZmiana;
}

inline bool Regulator::WylaczenieG_IN() const {
    return AutoZmiana_subState == WylaczenieG;
}

inline bool Regulator::AutoZmiana_Wylaczenie_IN() const {
    return AutoZmiana_subState == AutoZmiana_Wylaczenie;
}

inline bool Regulator::AutoZmiana_Wlaczenie_IN() const {
    return AutoZmiana_subState == AutoZmiana_Wlaczenie;
}

inline bool Regulator::UstalCzas_IN() const {
    return AutoZmiana_subState == UstalCzas;
}

inline bool Regulator::AutoZmiana_SprawdzenieTemp_IN() const {
    return AutoZmiana_subState == AutoZmiana_SprawdzenieTemp;
}

inline bool Regulator::SprawdzenieCzas_IN() const {
    return AutoZmiana_subState == SprawdzenieCzas;
}

inline bool Regulator::AutoZmiana_Grzanie_IN() const {
    return AutoZmiana_subState == AutoZmiana_Grzanie;
}

inline bool Regulator::AutoZmiana_Chlodzenie_IN() const {
    return AutoZmiana_subState == AutoZmiana_Chlodzenie;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Regulator.h
*********************************************************************/
