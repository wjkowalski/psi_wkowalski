/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Serwisant
//!	Generated Date	: Tue, 10, Sep 2019  
	File Path	: DefaultComponent/DefaultConfig/Serwisant.h
*********************************************************************/

#ifndef Serwisant_H
#define Serwisant_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## actor Serwisant
#include "Operator.h"
//## package Default

//## actor Serwisant
class Serwisant : public Operator {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedSerwisant;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Serwisant();
    
    //## auto_generated
    ~Serwisant();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedSerwisant : public OMAnimatedOperator {
    DECLARE_META(Serwisant, OMAnimatedSerwisant)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Serwisant.h
*********************************************************************/
