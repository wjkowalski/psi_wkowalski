/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Czujnik
//!	Generated Date	: Tue, 10, Sep 2019  
	File Path	: DefaultComponent/DefaultConfig/Czujnik.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Czujnik.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define Default_Czujnik_Czujnik_SERIALIZE OM_NO_OP

#define Default_Czujnik_getOption_SERIALIZE OM_NO_OP

#define Default_Czujnik_off_SERIALIZE OM_NO_OP

#define Default_Czujnik_on_SERIALIZE OM_NO_OP

#define Default_Czujnik_setOption_SERIALIZE aomsmethod->addAttribute("option", x2String(option));
//#]

//## package Default

//## class Czujnik
Czujnik::Czujnik(IOxfActive* theActiveContext) : StanC(ON) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Czujnik, Czujnik(), 0, Default_Czujnik_Czujnik_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsSterownik = NULL;
}

Czujnik::~Czujnik() {
    NOTIFY_DESTRUCTOR(~Czujnik, false);
    cleanUpRelations();
}

RhpString Czujnik::getOption() {
    NOTIFY_OPERATION(getOption, getOption(), 0, Default_Czujnik_getOption_SERIALIZE);
    //#[ operation getOption()
    return "x";
    //#]
}

bool Czujnik::off() {
    NOTIFY_OPERATION(off, off(), 0, Default_Czujnik_off_SERIALIZE);
    //#[ operation off()
    return true;
    //#]
}

bool Czujnik::on() {
    NOTIFY_OPERATION(on, on(), 0, Default_Czujnik_on_SERIALIZE);
    //#[ operation on()
    return true;
    //#]
}

bool Czujnik::setOption(const RhpString& option) {
    NOTIFY_OPERATION(setOption, setOption(const RhpString&), 1, Default_Czujnik_setOption_SERIALIZE);
    //#[ operation setOption(RhpString)
    return true;
    //#]
}

Sterownik* Czujnik::getItsSterownik() const {
    return itsSterownik;
}

void Czujnik::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool Czujnik::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void Czujnik::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

Stan Czujnik::getStanC() const {
    return StanC;
}

void Czujnik::setStanC(Stan p_StanC) {
    StanC = p_StanC;
}

int Czujnik::getTempBiez() const {
    return TempBiez;
}

void Czujnik::setTempBiez(int p_TempBiez) {
    TempBiez = p_TempBiez;
}

void Czujnik::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void Czujnik::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void Czujnik::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedCzujnik::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("TempBiez", x2String(myReal->TempBiez));
    aomsAttributes->addAttribute("StanC", x2String((int)myReal->StanC));
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedCzujnik::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_REACTIVE_META_S_SIMPLE_P(Czujnik, Default, false, Modul, OMAnimatedModul, OMAnimatedCzujnik)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Czujnik.cpp
*********************************************************************/
