/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Modul
//!	Generated Date	: Tue, 10, Sep 2019  
	File Path	: DefaultComponent/DefaultConfig/Modul.h
*********************************************************************/

#ifndef Modul_H
#define Modul_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## package Default

//## class Modul
class Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedModul;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Modul();
    
    //## auto_generated
    virtual ~Modul();
    
    ////    Operations    ////
    
    //## operation getOption()
    virtual RhpString getOption();
    
    //## operation off()
    virtual bool off();
    
    //## operation on()
    virtual bool on();
    
    //## operation setOption(RhpString)
    virtual bool setOption(const RhpString& option);
    
    ////    Additional operations    ////

protected :

    //## auto_generated
    int getID() const;
    
    //## auto_generated
    void setID(int p_ID);
    
    //## auto_generated
    bool getAvalible() const;
    
    //## auto_generated
    void setAvalible(bool p_avalible);
    
    //## auto_generated
    RhpString getName() const;
    
    //## auto_generated
    void setName(RhpString p_name);
    
    ////    Attributes    ////
    
    int ID;		//## attribute ID
    
    bool avalible;		//## attribute avalible
    
    RhpString name;		//## attribute name
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedModul : virtual public AOMInstance {
    DECLARE_META(Modul, OMAnimatedModul)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Modul.h
*********************************************************************/
