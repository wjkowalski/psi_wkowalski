/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Rejestr
//!	Generated Date	: Tue, 10, Sep 2019  
	File Path	: DefaultComponent/DefaultConfig/Rejestr.h
*********************************************************************/

#ifndef Rejestr_H
#define Rejestr_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Rejestr
#include "Modul.h"
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include <oxf/omthread.h>
//## link itsSterownik
class Sterownik;

//## package Default

//## class Rejestr
class Rejestr : public OMReactive, public Modul {
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedRejestr;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Rejestr(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    virtual ~Rejestr();
    
    ////    Operations    ////
    
    //## operation evReplyRejestr()
    void evReplyRejestr();
    
    //## operation getOption()
    virtual RhpString getOption();
    
    //## operation off()
    virtual bool off();
    
    //## operation on()
    virtual bool on();
    
    //## operation setOption(RhpString)
    virtual bool setOption(const RhpString& option);
    
    ////    Additional operations    ////
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    void setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void cleanUpRelations();

private :

    //## auto_generated
    int getCzasR() const;
    
    //## auto_generated
    void setCzasR(int p_CzasR);
    
    //## auto_generated
    int getZdarzenie() const;
    
    //## auto_generated
    void setZdarzenie(int p_Zdarzenie);
    
    ////    Attributes    ////

protected :

    int CzasR;		//## attribute CzasR
    
    int Zdarzenie;		//## attribute Zdarzenie
    
    ////    Relations and components    ////
    
    Sterownik* itsSterownik;		//## link itsSterownik
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _clearItsSterownik();
    
    ////    Framework    ////
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedRejestr : public OMAnimatedModul {
    DECLARE_META(Rejestr, OMAnimatedRejestr)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Rejestr.h
*********************************************************************/
