/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Sterownik
//!	Generated Date	: Tue, 10, Sep 2019  
	File Path	: DefaultComponent/DefaultConfig/Sterownik.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Sterownik.h"
//## link itsRegulator
#include "Regulator.h"
//#[ ignore
#define Default_Sterownik_Sterownik_SERIALIZE OM_NO_OP

#define OMAnim_Default_Sterownik_setAutozmiana_Stan_UNSERIALIZE_ARGS OP_UNSER(OMDestructiveString2X,(int&)p_autozmiana)

#define OMAnim_Default_Sterownik_setAutozmiana_Stan_SERIALIZE_RET_VAL
//#]

//## package Default

//## class Sterownik
Sterownik::~Sterownik() {
    NOTIFY_DESTRUCTOR(~Sterownik, true);
    cleanUpRelations();
    cancelTimeouts();
}

Czujnik* Sterownik::getItsCzujnik() const {
    return (Czujnik*) &itsCzujnik;
}

Grzejnik* Sterownik::getItsGrzejnik() const {
    return (Grzejnik*) &itsGrzejnik;
}

PanelKontrolny* Sterownik::getItsPanelKontrolny() const {
    return (PanelKontrolny*) &itsPanelKontrolny;
}

Regulator* Sterownik::getItsRegulator() const {
    return itsRegulator;
}

void Sterownik::setItsRegulator(Regulator* p_Regulator) {
    _setItsRegulator(p_Regulator);
}

Rejestr* Sterownik::getItsRejestr() const {
    return (Rejestr*) &itsRejestr;
}

bool Sterownik::startBehavior() {
    bool done = true;
    done &= itsCzujnik.startBehavior();
    done &= itsGrzejnik.startBehavior();
    done &= itsPanelKontrolny.startBehavior();
    done &= itsRejestr.startBehavior();
    done &= OMReactive::startBehavior();
    return done;
}

void Sterownik::initRelations() {
    itsCzujnik._setItsSterownik(this);
    itsGrzejnik._setItsSterownik(this);
    itsPanelKontrolny._setItsSterownik(this);
    itsRejestr._setItsSterownik(this);
}

void Sterownik::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
    ZmianaUp_subState = OMNonState;
    ZmianaDown_subState = OMNonState;
    StanObecny_subState = OMNonState;
    SprawdzenieRejestru_subState = OMNonState;
    SprawdzenieRejestru_timeout = NULL;
    rootState_timeout = NULL;
    Autozmiana_subState = OMNonState;
}

void Sterownik::cleanUpRelations() {
    if(itsRegulator != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsRegulator");
            itsRegulator = NULL;
        }
}

void Sterownik::cancelTimeouts() {
    cancel(SprawdzenieRejestru_timeout);
    cancel(rootState_timeout);
}

bool Sterownik::cancelTimeout(const IOxfTimeout* arg) {
    bool res = false;
    if(SprawdzenieRejestru_timeout == arg)
        {
            SprawdzenieRejestru_timeout = NULL;
            res = true;
        }
    if(rootState_timeout == arg)
        {
            rootState_timeout = NULL;
            res = true;
        }
    return res;
}

int Sterownik::getCzas() const {
    return Czas;
}

void Sterownik::setCzas(int p_Czas) {
    Czas = p_Czas;
    NOTIFY_SET_OPERATION;
}

int Sterownik::getCzasZmiany() const {
    return CzasZmiany;
}

void Sterownik::setCzasZmiany(int p_CzasZmiany) {
    CzasZmiany = p_CzasZmiany;
    NOTIFY_SET_OPERATION;
}

float Sterownik::getTempBiezS() const {
    return TempBiezS;
}

void Sterownik::setTempBiezS(float p_TempBiezS) {
    TempBiezS = p_TempBiezS;
    NOTIFY_SET_OPERATION;
}

float Sterownik::getTempZadana() const {
    return TempZadana;
}

void Sterownik::setTempZadana(float p_TempZadana) {
    TempZadana = p_TempZadana;
    NOTIFY_SET_OPERATION;
}

AutomatZmiana Sterownik::getAutooperacja() const {
    return autooperacja;
}

void Sterownik::setAutooperacja(AutomatZmiana p_autooperacja) {
    autooperacja = p_autooperacja;
}

Stan Sterownik::getAutozmiana() const {
    return autozmiana;
}

void Sterownik::setAutozmiana(Stan p_autozmiana) {
    autozmiana = p_autozmiana;
    NOTIFY_SET_OPERATION;
}

Operacja Sterownik::getOperacje() const {
    return operacje;
}

void Sterownik::setOperacje(Operacja p_operacje) {
    operacje = p_operacje;
}

void Sterownik::__setItsRegulator(Regulator* p_Regulator) {
    itsRegulator = p_Regulator;
    if(p_Regulator != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsRegulator", p_Regulator, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsRegulator");
        }
}

void Sterownik::_setItsRegulator(Regulator* p_Regulator) {
    __setItsRegulator(p_Regulator);
}

void Sterownik::_clearItsRegulator() {
    NOTIFY_RELATION_CLEARED("itsRegulator");
    itsRegulator = NULL;
}

void Sterownik::setActiveContext(IOxfActive* theActiveContext, bool activeInstance) {
    OMReactive::setActiveContext(theActiveContext, activeInstance);
    {
        itsPanelKontrolny.setActiveContext(theActiveContext, false);
        itsRejestr.setActiveContext(theActiveContext, false);
        itsCzujnik.setActiveContext(theActiveContext, false);
        itsGrzejnik.setActiveContext(theActiveContext, false);
    }
}

void Sterownik::destroy() {
    itsCzujnik.destroy();
    itsGrzejnik.destroy();
    itsPanelKontrolny.destroy();
    itsRejestr.destroy();
    OMReactive::destroy();
}

Sterownik::Sterownik(IOxfActive* theActiveContext) : Czas(0), CzasZmiany(0), TempBiezS(20), TempZadana(20), autozmiana(OFF) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Sterownik, Sterownik(), 0, Default_Sterownik_Sterownik_SERIALIZE);
    setActiveContext(theActiveContext, false);
    {
        {
            itsPanelKontrolny.setShouldDelete(false);
        }
        {
            itsRejestr.setShouldDelete(false);
        }
        {
            itsCzujnik.setShouldDelete(false);
        }
        {
            itsGrzejnik.setShouldDelete(false);
        }
    }
    itsRegulator = NULL;
    initRelations();
    initStatechart();
}

void Sterownik::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("21");
        NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
        rootState_subState = Oczekiwanie;
        rootState_active = Oczekiwanie;
        rootState_timeout = scheduleTimeout(300, "ROOT.Oczekiwanie");
        NOTIFY_TRANSITION_TERMINATED("21");
    }
}

IOxfReactive::TakeEventStatus Sterownik::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Oczekiwanie
        case Oczekiwanie:
        {
            res = Oczekiwanie_handleEvent();
        }
        break;
        // State sendaction_21
        case sendaction_21:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 43 
                    if((Czas==CzasZmiany))
                        {
                            NOTIFY_TRANSITION_STARTED("43");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Autozmiana.sendaction_21");
                            NOTIFY_STATE_ENTERED("ROOT.Autozmiana.sendaction_10");
                            pushNullTransition();
                            Autozmiana_subState = sendaction_10;
                            rootState_active = sendaction_10;
                            //#[ state Autozmiana.sendaction_10.(Entry) 
                            itsGrzejnik.GEN(evZadajTemp);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("43");
                            res = eventConsumed;
                        }
                }
            
            
        }
        break;
        // State sendaction_22
        case sendaction_22:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 42 
                    if((Czas==CzasZmiany))
                        {
                            NOTIFY_TRANSITION_STARTED("42");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Autozmiana.sendaction_22");
                            NOTIFY_STATE_ENTERED("ROOT.Autozmiana.sendaction_10");
                            pushNullTransition();
                            Autozmiana_subState = sendaction_10;
                            rootState_active = sendaction_10;
                            //#[ state Autozmiana.sendaction_10.(Entry) 
                            itsGrzejnik.GEN(evZadajTemp);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("42");
                            res = eventConsumed;
                        }
                }
            
            
        }
        break;
        // State sendaction_10
        case sendaction_10:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 9 
                    if(operacje==ZmianaDown&&TempZadana<TempBiezS)
                        {
                            NOTIFY_TRANSITION_STARTED("17");
                            NOTIFY_TRANSITION_STARTED("9");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Autozmiana.sendaction_10");
                            NOTIFY_STATE_ENTERED("ROOT.Autozmiana.sendaction_13");
                            pushNullTransition();
                            Autozmiana_subState = sendaction_13;
                            rootState_active = sendaction_13;
                            //#[ state Autozmiana.sendaction_13.(Entry) 
                            itsGrzejnik.GEN(evStop);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("9");
                            NOTIFY_TRANSITION_TERMINATED("17");
                            res = eventConsumed;
                        }
                    else
                        {
                            //## transition 10 
                            if(operacje==ZmianaUp||operacje==ZmianaDown&&TempZadana<TempBiezS)
                                {
                                    NOTIFY_TRANSITION_STARTED("17");
                                    NOTIFY_TRANSITION_STARTED("10");
                                    popNullTransition();
                                    NOTIFY_STATE_EXITED("ROOT.Autozmiana.sendaction_10");
                                    NOTIFY_STATE_ENTERED("ROOT.Autozmiana.sendaction_12");
                                    pushNullTransition();
                                    Autozmiana_subState = sendaction_12;
                                    rootState_active = sendaction_12;
                                    //#[ state Autozmiana.sendaction_12.(Entry) 
                                    itsGrzejnik.GEN(evStart);
                                    //#]
                                    NOTIFY_TRANSITION_TERMINATED("10");
                                    NOTIFY_TRANSITION_TERMINATED("17");
                                    res = eventConsumed;
                                }
                        }
                }
            
            
        }
        break;
        // State sendaction_13
        case sendaction_13:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("20");
                    Autozmiana_exit();
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    rootState_timeout = scheduleTimeout(300, "ROOT.Oczekiwanie");
                    NOTIFY_TRANSITION_TERMINATED("20");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State sendaction_12
        case sendaction_12:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("19");
                    Autozmiana_exit();
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    rootState_timeout = scheduleTimeout(300, "ROOT.Oczekiwanie");
                    NOTIFY_TRANSITION_TERMINATED("19");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State sendaction_10
        case ZmianaUp_sendaction_10:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 4 
                    if(TempBiezS<TempZadana)
                        {
                            NOTIFY_TRANSITION_STARTED("28");
                            NOTIFY_TRANSITION_STARTED("4");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.ZmianaUp.sendaction_10");
                            NOTIFY_STATE_ENTERED("ROOT.ZmianaUp.sendaction_12");
                            pushNullTransition();
                            ZmianaUp_subState = ZmianaUp_sendaction_12;
                            rootState_active = ZmianaUp_sendaction_12;
                            //#[ state ZmianaUp.sendaction_12.(Entry) 
                            itsGrzejnik.GEN(evStart);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("4");
                            NOTIFY_TRANSITION_TERMINATED("28");
                            res = eventConsumed;
                        }
                    else
                        {
                            //## transition 24 
                            if(TempBiezS>=TempZadana)
                                {
                                    NOTIFY_TRANSITION_STARTED("28");
                                    NOTIFY_TRANSITION_STARTED("24");
                                    popNullTransition();
                                    NOTIFY_STATE_EXITED("ROOT.ZmianaUp.sendaction_10");
                                    NOTIFY_STATE_ENTERED("ROOT.ZmianaUp.sendaction_31");
                                    pushNullTransition();
                                    ZmianaUp_subState = sendaction_31;
                                    rootState_active = sendaction_31;
                                    //#[ state ZmianaUp.sendaction_31.(Entry) 
                                    itsGrzejnik.GEN(evStop);
                                    //#]
                                    NOTIFY_TRANSITION_TERMINATED("24");
                                    NOTIFY_TRANSITION_TERMINATED("28");
                                    res = eventConsumed;
                                }
                        }
                }
            
            
        }
        break;
        // State sendaction_12
        case ZmianaUp_sendaction_12:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("25");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.ZmianaUp.sendaction_12");
                    NOTIFY_STATE_ENTERED("ROOT.ZmianaUp.accepteventaction_39");
                    pushNullTransition();
                    ZmianaUp_subState = accepteventaction_39;
                    rootState_active = accepteventaction_39;
                    NOTIFY_TRANSITION_TERMINATED("25");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State sendaction_31
        case sendaction_31:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("23");
                    ZmianaUp_exit();
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    rootState_timeout = scheduleTimeout(300, "ROOT.Oczekiwanie");
                    NOTIFY_TRANSITION_TERMINATED("23");
                    res = eventConsumed;
                }
            
            
        }
        break;
        case accepteventaction_39:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 26 
                    if(TempBiezS>=TempZadana)
                        {
                            NOTIFY_TRANSITION_STARTED("36");
                            NOTIFY_TRANSITION_STARTED("26");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.ZmianaUp.accepteventaction_39");
                            NOTIFY_STATE_ENTERED("ROOT.ZmianaUp.sendaction_31");
                            pushNullTransition();
                            ZmianaUp_subState = sendaction_31;
                            rootState_active = sendaction_31;
                            //#[ state ZmianaUp.sendaction_31.(Entry) 
                            itsGrzejnik.GEN(evStop);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("26");
                            NOTIFY_TRANSITION_TERMINATED("36");
                            res = eventConsumed;
                        }
                    else
                        {
                            //## transition 27 
                            if(TempBiezS<TempZadana)
                                {
                                    NOTIFY_TRANSITION_STARTED("36");
                                    NOTIFY_TRANSITION_STARTED("27");
                                    popNullTransition();
                                    NOTIFY_STATE_EXITED("ROOT.ZmianaUp.accepteventaction_39");
                                    NOTIFY_STATE_ENTERED("ROOT.ZmianaUp.accepteventaction_39");
                                    pushNullTransition();
                                    ZmianaUp_subState = accepteventaction_39;
                                    rootState_active = accepteventaction_39;
                                    NOTIFY_TRANSITION_TERMINATED("27");
                                    NOTIFY_TRANSITION_TERMINATED("36");
                                    res = eventConsumed;
                                }
                        }
                }
            
            
        }
        break;
        // State sendaction_16
        case sendaction_16:
        {
            if(IS_EVENT_TYPE_OF(evGetTime_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("22");
                    NOTIFY_STATE_EXITED("ROOT.StanObecny.sendaction_16");
                    NOTIFY_STATE_ENTERED("ROOT.StanObecny.sendaction_18");
                    pushNullTransition();
                    StanObecny_subState = sendaction_18;
                    rootState_active = sendaction_18;
                    //#[ state StanObecny.sendaction_18.(Entry) 
                    itsRejestr.GEN(evSaveTime);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("22");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State sendaction_18
        case sendaction_18:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("15");
                    switch (StanObecny_subState) {
                        // State sendaction_16
                        case sendaction_16:
                        {
                            NOTIFY_STATE_EXITED("ROOT.StanObecny.sendaction_16");
                        }
                        break;
                        // State sendaction_18
                        case sendaction_18:
                        {
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.StanObecny.sendaction_18");
                        }
                        break;
                        default:
                            break;
                    }
                    StanObecny_subState = OMNonState;
                    NOTIFY_STATE_EXITED("ROOT.StanObecny");
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    rootState_timeout = scheduleTimeout(300, "ROOT.Oczekiwanie");
                    NOTIFY_TRANSITION_TERMINATED("15");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State sendaction_28
        case sendaction_28:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == SprawdzenieRejestru_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("12");
                            SprawdzenieRejestru_exit();
                            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                            rootState_subState = Oczekiwanie;
                            rootState_active = Oczekiwanie;
                            rootState_timeout = scheduleTimeout(300, "ROOT.Oczekiwanie");
                            NOTIFY_TRANSITION_TERMINATED("12");
                            res = eventConsumed;
                        }
                }
            
            
        }
        break;
        // State sendaction_35
        case sendaction_35:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 32 
                    if(TempBiezS<=TempZadana)
                        {
                            NOTIFY_TRANSITION_STARTED("31");
                            NOTIFY_TRANSITION_STARTED("32");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.ZmianaDown.sendaction_35");
                            NOTIFY_STATE_ENTERED("ROOT.ZmianaDown.sendaction_36");
                            pushNullTransition();
                            ZmianaDown_subState = sendaction_36;
                            rootState_active = sendaction_36;
                            //#[ state ZmianaDown.sendaction_36.(Entry) 
                            itsGrzejnik.GEN(evStop);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("32");
                            NOTIFY_TRANSITION_TERMINATED("31");
                            res = eventConsumed;
                        }
                    else
                        {
                            //## transition 33 
                            if(TempBiezS>TempZadana)
                                {
                                    NOTIFY_TRANSITION_STARTED("31");
                                    NOTIFY_TRANSITION_STARTED("33");
                                    popNullTransition();
                                    NOTIFY_STATE_EXITED("ROOT.ZmianaDown.sendaction_35");
                                    NOTIFY_STATE_ENTERED("ROOT.ZmianaDown.sendaction_37");
                                    pushNullTransition();
                                    ZmianaDown_subState = sendaction_37;
                                    rootState_active = sendaction_37;
                                    //#[ state ZmianaDown.sendaction_37.(Entry) 
                                    itsGrzejnik.GEN(evStart);
                                    //#]
                                    NOTIFY_TRANSITION_TERMINATED("33");
                                    NOTIFY_TRANSITION_TERMINATED("31");
                                    res = eventConsumed;
                                }
                        }
                }
            
            
        }
        break;
        // State sendaction_36
        case sendaction_36:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("34");
                    ZmianaDown_exit();
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    rootState_timeout = scheduleTimeout(300, "ROOT.Oczekiwanie");
                    NOTIFY_TRANSITION_TERMINATED("34");
                    res = eventConsumed;
                }
            
            
        }
        break;
        // State sendaction_37
        case sendaction_37:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("40");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.ZmianaDown.sendaction_37");
                    NOTIFY_STATE_ENTERED("ROOT.ZmianaDown.accepteventaction_39");
                    pushNullTransition();
                    ZmianaDown_subState = ZmianaDown_accepteventaction_39;
                    rootState_active = ZmianaDown_accepteventaction_39;
                    NOTIFY_TRANSITION_TERMINATED("40");
                    res = eventConsumed;
                }
            
            
        }
        break;
        case ZmianaDown_accepteventaction_39:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 39 
                    if(TempBiezS<=TempZadana)
                        {
                            NOTIFY_TRANSITION_STARTED("38");
                            NOTIFY_TRANSITION_STARTED("39");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.ZmianaDown.accepteventaction_39");
                            NOTIFY_STATE_ENTERED("ROOT.ZmianaDown.sendaction_36");
                            pushNullTransition();
                            ZmianaDown_subState = sendaction_36;
                            rootState_active = sendaction_36;
                            //#[ state ZmianaDown.sendaction_36.(Entry) 
                            itsGrzejnik.GEN(evStop);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("39");
                            NOTIFY_TRANSITION_TERMINATED("38");
                            res = eventConsumed;
                        }
                    else
                        {
                            //## transition 41 
                            if(TempBiezS>TempZadana)
                                {
                                    NOTIFY_TRANSITION_STARTED("38");
                                    NOTIFY_TRANSITION_STARTED("41");
                                    popNullTransition();
                                    NOTIFY_STATE_EXITED("ROOT.ZmianaDown.accepteventaction_39");
                                    NOTIFY_STATE_ENTERED("ROOT.ZmianaDown.accepteventaction_39");
                                    pushNullTransition();
                                    ZmianaDown_subState = ZmianaDown_accepteventaction_39;
                                    rootState_active = ZmianaDown_accepteventaction_39;
                                    NOTIFY_TRANSITION_TERMINATED("41");
                                    NOTIFY_TRANSITION_TERMINATED("38");
                                    res = eventConsumed;
                                }
                        }
                }
            
            
        }
        break;
        default:
            break;
    }
    return res;
}

void Sterownik::ZmianaUp_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.ZmianaUp");
    rootState_subState = ZmianaUp;
    NOTIFY_TRANSITION_STARTED("16");
    NOTIFY_STATE_ENTERED("ROOT.ZmianaUp.sendaction_10");
    pushNullTransition();
    ZmianaUp_subState = ZmianaUp_sendaction_10;
    rootState_active = ZmianaUp_sendaction_10;
    //#[ state ZmianaUp.sendaction_10.(Entry) 
    itsGrzejnik.GEN(evZadajTemp);
    //#]
    NOTIFY_TRANSITION_TERMINATED("16");
}

void Sterownik::ZmianaUp_exit() {
    switch (ZmianaUp_subState) {
        // State sendaction_10
        case ZmianaUp_sendaction_10:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.ZmianaUp.sendaction_10");
        }
        break;
        // State sendaction_12
        case ZmianaUp_sendaction_12:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.ZmianaUp.sendaction_12");
        }
        break;
        // State sendaction_31
        case sendaction_31:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.ZmianaUp.sendaction_31");
        }
        break;
        case accepteventaction_39:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.ZmianaUp.accepteventaction_39");
        }
        break;
        default:
            break;
    }
    ZmianaUp_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.ZmianaUp");
}

void Sterownik::ZmianaDown_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.ZmianaDown");
    rootState_subState = ZmianaDown;
    NOTIFY_TRANSITION_STARTED("30");
    NOTIFY_STATE_ENTERED("ROOT.ZmianaDown.sendaction_35");
    pushNullTransition();
    ZmianaDown_subState = sendaction_35;
    rootState_active = sendaction_35;
    //#[ state ZmianaDown.sendaction_35.(Entry) 
    itsGrzejnik.GEN(evZadajTemp);
    //#]
    NOTIFY_TRANSITION_TERMINATED("30");
}

void Sterownik::ZmianaDown_exit() {
    switch (ZmianaDown_subState) {
        // State sendaction_35
        case sendaction_35:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.ZmianaDown.sendaction_35");
        }
        break;
        // State sendaction_36
        case sendaction_36:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.ZmianaDown.sendaction_36");
        }
        break;
        // State sendaction_37
        case sendaction_37:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.ZmianaDown.sendaction_37");
        }
        break;
        case ZmianaDown_accepteventaction_39:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.ZmianaDown.accepteventaction_39");
        }
        break;
        default:
            break;
    }
    ZmianaDown_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.ZmianaDown");
}

void Sterownik::StanObecny_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.StanObecny");
    rootState_subState = StanObecny;
    NOTIFY_TRANSITION_STARTED("14");
    NOTIFY_STATE_ENTERED("ROOT.StanObecny.sendaction_16");
    StanObecny_subState = sendaction_16;
    rootState_active = sendaction_16;
    //#[ state StanObecny.sendaction_16.(Entry) 
    itsCzujnik.GEN(evGetTemp);
    //#]
    NOTIFY_TRANSITION_TERMINATED("14");
}

void Sterownik::SprawdzenieRejestru_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.SprawdzenieRejestru");
    rootState_subState = SprawdzenieRejestru;
    NOTIFY_TRANSITION_STARTED("18");
    NOTIFY_STATE_ENTERED("ROOT.SprawdzenieRejestru.sendaction_28");
    SprawdzenieRejestru_subState = sendaction_28;
    rootState_active = sendaction_28;
    //#[ state SprawdzenieRejestru.sendaction_28.(Entry) 
    itsRejestr.GEN(evAskRejestr);
    //#]
    SprawdzenieRejestru_timeout = scheduleTimeout(300, "ROOT.SprawdzenieRejestru.sendaction_28");
    NOTIFY_TRANSITION_TERMINATED("18");
}

void Sterownik::SprawdzenieRejestru_exit() {
    // State sendaction_28
    if(SprawdzenieRejestru_subState == sendaction_28)
        {
            cancel(SprawdzenieRejestru_timeout);
            NOTIFY_STATE_EXITED("ROOT.SprawdzenieRejestru.sendaction_28");
        }
    SprawdzenieRejestru_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.SprawdzenieRejestru");
}

IOxfReactive::TakeEventStatus Sterownik::Oczekiwanie_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(evKeyDownPressed_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("29");
            cancel(rootState_timeout);
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            ZmianaDown_entDef();
            NOTIFY_TRANSITION_TERMINATED("29");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
        {
            if(getCurrentEvent() == rootState_timeout)
                {
                    NOTIFY_TRANSITION_STARTED("5");
                    cancel(rootState_timeout);
                    NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
                    StanObecny_entDef();
                    NOTIFY_TRANSITION_TERMINATED("5");
                    res = eventConsumed;
                }
        }
    else if(IS_EVENT_TYPE_OF(evKeyUpPressed_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("3");
            cancel(rootState_timeout);
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            ZmianaUp_entDef();
            NOTIFY_TRANSITION_TERMINATED("3");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evKeyRejestr_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("2");
            cancel(rootState_timeout);
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            SprawdzenieRejestru_entDef();
            NOTIFY_TRANSITION_TERMINATED("2");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evAutoTempKey_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("0");
            cancel(rootState_timeout);
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            Autozmiana_entDef();
            NOTIFY_TRANSITION_TERMINATED("0");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evSprawdzStan_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("1");
            cancel(rootState_timeout);
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            StanObecny_entDef();
            NOTIFY_TRANSITION_TERMINATED("1");
            res = eventConsumed;
        }
    
    return res;
}

void Sterownik::Autozmiana_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Autozmiana");
    rootState_subState = Autozmiana;
    AutozmianaEntDef();
}

void Sterownik::AutozmianaEntDef() {
    //## transition 7 
    if(operacje==ZmianaUp)
        {
            NOTIFY_TRANSITION_STARTED("6");
            NOTIFY_TRANSITION_STARTED("7");
            NOTIFY_STATE_ENTERED("ROOT.Autozmiana.sendaction_21");
            pushNullTransition();
            Autozmiana_subState = sendaction_21;
            rootState_active = sendaction_21;
            //#[ state Autozmiana.sendaction_21.(Entry) 
            GEN(evZwiekszTemp);
            //#]
            NOTIFY_TRANSITION_TERMINATED("7");
            NOTIFY_TRANSITION_TERMINATED("6");
        }
    else
        {
            //## transition 8 
            if(operacje==ZmianaDown)
                {
                    NOTIFY_TRANSITION_STARTED("6");
                    NOTIFY_TRANSITION_STARTED("8");
                    NOTIFY_STATE_ENTERED("ROOT.Autozmiana.sendaction_22");
                    pushNullTransition();
                    Autozmiana_subState = sendaction_22;
                    rootState_active = sendaction_22;
                    //#[ state Autozmiana.sendaction_22.(Entry) 
                    GEN(evZmniejszTemp);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("8");
                    NOTIFY_TRANSITION_TERMINATED("6");
                }
        }
}

void Sterownik::Autozmiana_exit() {
    switch (Autozmiana_subState) {
        // State sendaction_21
        case sendaction_21:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Autozmiana.sendaction_21");
        }
        break;
        // State sendaction_22
        case sendaction_22:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Autozmiana.sendaction_22");
        }
        break;
        // State sendaction_10
        case sendaction_10:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Autozmiana.sendaction_10");
        }
        break;
        // State sendaction_13
        case sendaction_13:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Autozmiana.sendaction_13");
        }
        break;
        // State sendaction_12
        case sendaction_12:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Autozmiana.sendaction_12");
        }
        break;
        default:
            break;
    }
    Autozmiana_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.Autozmiana");
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedSterownik::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("operacje", x2String((int)myReal->operacje));
    aomsAttributes->addAttribute("TempZadana", x2String(myReal->TempZadana));
    aomsAttributes->addAttribute("Czas", x2String(myReal->Czas));
    aomsAttributes->addAttribute("CzasZmiany", x2String(myReal->CzasZmiany));
    aomsAttributes->addAttribute("autooperacja", x2String((int)myReal->autooperacja));
    aomsAttributes->addAttribute("autozmiana", x2String((int)myReal->autozmiana));
    aomsAttributes->addAttribute("TempBiezS", x2String(myReal->TempBiezS));
}

void OMAnimatedSterownik::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsPanelKontrolny", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsPanelKontrolny);
    aomsRelations->addRelation("itsRejestr", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsRejestr);
    aomsRelations->addRelation("itsCzujnik", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsCzujnik);
    aomsRelations->addRelation("itsGrzejnik", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsGrzejnik);
    aomsRelations->addRelation("itsRegulator", false, true);
    if(myReal->itsRegulator)
        {
            aomsRelations->ADD_ITEM(myReal->itsRegulator);
        }
}

void OMAnimatedSterownik::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Sterownik::Oczekiwanie:
        {
            Oczekiwanie_serializeStates(aomsState);
        }
        break;
        case Sterownik::Autozmiana:
        {
            Autozmiana_serializeStates(aomsState);
        }
        break;
        case Sterownik::ZmianaUp:
        {
            ZmianaUp_serializeStates(aomsState);
        }
        break;
        case Sterownik::StanObecny:
        {
            StanObecny_serializeStates(aomsState);
        }
        break;
        case Sterownik::SprawdzenieRejestru:
        {
            SprawdzenieRejestru_serializeStates(aomsState);
        }
        break;
        case Sterownik::ZmianaDown:
        {
            ZmianaDown_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedSterownik::ZmianaUp_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.ZmianaUp");
    switch (myReal->ZmianaUp_subState) {
        case Sterownik::ZmianaUp_sendaction_10:
        {
            ZmianaUp_sendaction_10_serializeStates(aomsState);
        }
        break;
        case Sterownik::ZmianaUp_sendaction_12:
        {
            ZmianaUp_sendaction_12_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_31:
        {
            sendaction_31_serializeStates(aomsState);
        }
        break;
        case Sterownik::accepteventaction_39:
        {
            accepteventaction_39_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedSterownik::sendaction_31_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.ZmianaUp.sendaction_31");
}

void OMAnimatedSterownik::ZmianaUp_sendaction_12_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.ZmianaUp.sendaction_12");
}

void OMAnimatedSterownik::ZmianaUp_sendaction_10_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.ZmianaUp.sendaction_10");
}

void OMAnimatedSterownik::accepteventaction_39_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.ZmianaUp.accepteventaction_39");
}

void OMAnimatedSterownik::ZmianaDown_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.ZmianaDown");
    switch (myReal->ZmianaDown_subState) {
        case Sterownik::sendaction_35:
        {
            sendaction_35_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_36:
        {
            sendaction_36_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_37:
        {
            sendaction_37_serializeStates(aomsState);
        }
        break;
        case Sterownik::ZmianaDown_accepteventaction_39:
        {
            ZmianaDown_accepteventaction_39_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedSterownik::sendaction_37_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.ZmianaDown.sendaction_37");
}

void OMAnimatedSterownik::sendaction_36_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.ZmianaDown.sendaction_36");
}

void OMAnimatedSterownik::sendaction_35_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.ZmianaDown.sendaction_35");
}

void OMAnimatedSterownik::ZmianaDown_accepteventaction_39_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.ZmianaDown.accepteventaction_39");
}

void OMAnimatedSterownik::StanObecny_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.StanObecny");
    switch (myReal->StanObecny_subState) {
        case Sterownik::sendaction_16:
        {
            sendaction_16_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_18:
        {
            sendaction_18_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedSterownik::sendaction_18_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.StanObecny.sendaction_18");
}

void OMAnimatedSterownik::sendaction_16_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.StanObecny.sendaction_16");
}

void OMAnimatedSterownik::SprawdzenieRejestru_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.SprawdzenieRejestru");
    if(myReal->SprawdzenieRejestru_subState == Sterownik::sendaction_28)
        {
            sendaction_28_serializeStates(aomsState);
        }
}

void OMAnimatedSterownik::sendaction_28_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.SprawdzenieRejestru.sendaction_28");
}

void OMAnimatedSterownik::Oczekiwanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Oczekiwanie");
}

void OMAnimatedSterownik::Autozmiana_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Autozmiana");
    switch (myReal->Autozmiana_subState) {
        case Sterownik::sendaction_21:
        {
            sendaction_21_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_22:
        {
            sendaction_22_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_10:
        {
            sendaction_10_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_13:
        {
            sendaction_13_serializeStates(aomsState);
        }
        break;
        case Sterownik::sendaction_12:
        {
            sendaction_12_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedSterownik::sendaction_22_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Autozmiana.sendaction_22");
}

void OMAnimatedSterownik::sendaction_21_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Autozmiana.sendaction_21");
}

void OMAnimatedSterownik::sendaction_13_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Autozmiana.sendaction_13");
}

void OMAnimatedSterownik::sendaction_12_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Autozmiana.sendaction_12");
}

void OMAnimatedSterownik::sendaction_10_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Autozmiana.sendaction_10");
}
//#]

IMPLEMENT_REACTIVE_META_P(Sterownik, Default, Default, false, OMAnimatedSterownik)

IMPLEMENT_META_OP(OMAnimatedSterownik, Default_Sterownik_setAutozmiana_Stan, "setAutozmiana", FALSE, "setAutozmiana(Stan)", 1)

IMPLEMENT_OP_CALL(Default_Sterownik_setAutozmiana_Stan, Sterownik, setAutozmiana(p_autozmiana), NO_OP())
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Sterownik.cpp
*********************************************************************/
