/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Default
//!	Generated Date	: Tue, 10, Sep 2019  
	File Path	: DefaultComponent/DefaultConfig/Default.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Default.h"
//## auto_generated
#include "Czujnik.h"
//## auto_generated
#include "Grzejnik.h"
//## auto_generated
#include "Modul.h"
//## auto_generated
#include "PanelKontrolny.h"
//## auto_generated
#include "Regulator.h"
//## auto_generated
#include "Rejestr.h"
//## auto_generated
#include "Sterownik.h"
//#[ ignore
#define evZmiana_SERIALIZE OM_NO_OP

#define evZmiana_UNSERIALIZE OM_NO_OP

#define evZmiana_CONSTRUCTOR evZmiana()

#define evRedukujG_SERIALIZE OM_NO_OP

#define evRedukujG_UNSERIALIZE OM_NO_OP

#define evRedukujG_CONSTRUCTOR evRedukujG()

#define evStop_SERIALIZE OM_NO_OP

#define evStop_UNSERIALIZE OM_NO_OP

#define evStop_CONSTRUCTOR evStop()

#define evZadajTemp_SERIALIZE OM_NO_OP

#define evZadajTemp_UNSERIALIZE OM_NO_OP

#define evZadajTemp_CONSTRUCTOR evZadajTemp()

#define evStart_SERIALIZE OM_NO_OP

#define evStart_UNSERIALIZE OM_NO_OP

#define evStart_CONSTRUCTOR evStart()

#define evZwiekszG_SERIALIZE OM_NO_OP

#define evZwiekszG_UNSERIALIZE OM_NO_OP

#define evZwiekszG_CONSTRUCTOR evZwiekszG()

#define evSprawdzStan_SERIALIZE OM_NO_OP

#define evSprawdzStan_UNSERIALIZE OM_NO_OP

#define evSprawdzStan_CONSTRUCTOR evSprawdzStan()

#define evGetTemp_SERIALIZE OM_NO_OP

#define evGetTemp_UNSERIALIZE OM_NO_OP

#define evGetTemp_CONSTRUCTOR evGetTemp()

#define evCzujnikKaput_SERIALIZE OM_NO_OP

#define evCzujnikKaput_UNSERIALIZE OM_NO_OP

#define evCzujnikKaput_CONSTRUCTOR evCzujnikKaput()

#define evResetujStan_SERIALIZE OM_NO_OP

#define evResetujStan_UNSERIALIZE OM_NO_OP

#define evResetujStan_CONSTRUCTOR evResetujStan()

#define evZmienCzas_SERIALIZE OM_NO_OP

#define evZmienCzas_UNSERIALIZE OM_NO_OP

#define evZmienCzas_CONSTRUCTOR evZmienCzas()

#define evZwiekszTemp_SERIALIZE OM_NO_OP

#define evZwiekszTemp_UNSERIALIZE OM_NO_OP

#define evZwiekszTemp_CONSTRUCTOR evZwiekszTemp()

#define evZmniejszTemp_SERIALIZE OM_NO_OP

#define evZmniejszTemp_UNSERIALIZE OM_NO_OP

#define evZmniejszTemp_CONSTRUCTOR evZmniejszTemp()

#define evGetTime_SERIALIZE OM_NO_OP

#define evGetTime_UNSERIALIZE OM_NO_OP

#define evGetTime_CONSTRUCTOR evGetTime()

#define evKeyUpPressed_SERIALIZE OM_NO_OP

#define evKeyUpPressed_UNSERIALIZE OM_NO_OP

#define evKeyUpPressed_CONSTRUCTOR evKeyUpPressed()

#define evAutoTempKey_SERIALIZE OM_NO_OP

#define evAutoTempKey_UNSERIALIZE OM_NO_OP

#define evAutoTempKey_CONSTRUCTOR evAutoTempKey()

#define evKeyTimeUp_SERIALIZE OM_NO_OP

#define evKeyTimeUp_UNSERIALIZE OM_NO_OP

#define evKeyTimeUp_CONSTRUCTOR evKeyTimeUp()

#define evKeyRejestr_SERIALIZE OM_NO_OP

#define evKeyRejestr_UNSERIALIZE OM_NO_OP

#define evKeyRejestr_CONSTRUCTOR evKeyRejestr()

#define evAskRejestr_SERIALIZE OM_NO_OP

#define evAskRejestr_UNSERIALIZE OM_NO_OP

#define evAskRejestr_CONSTRUCTOR evAskRejestr()

#define evSaveTime_SERIALIZE OM_NO_OP

#define evSaveTime_UNSERIALIZE OM_NO_OP

#define evSaveTime_CONSTRUCTOR evSaveTime()

#define evAutoKeyPressed_SERIALIZE OM_NO_OP

#define evAutoKeyPressed_UNSERIALIZE OM_NO_OP

#define evAutoKeyPressed_CONSTRUCTOR evAutoKeyPressed()

#define evAwaria_SERIALIZE OM_NO_OP

#define evAwaria_UNSERIALIZE OM_NO_OP

#define evAwaria_CONSTRUCTOR evAwaria()

#define evKeyDownPressed_SERIALIZE OM_NO_OP

#define evKeyDownPressed_UNSERIALIZE OM_NO_OP

#define evKeyDownPressed_CONSTRUCTOR evKeyDownPressed()

#define evKeyTimeDown_SERIALIZE OM_NO_OP

#define evKeyTimeDown_UNSERIALIZE OM_NO_OP

#define evKeyTimeDown_CONSTRUCTOR evKeyTimeDown()

#define evReplyRejestr_SERIALIZE OM_NO_OP

#define evReplyRejestr_UNSERIALIZE OM_NO_OP

#define evReplyRejestr_CONSTRUCTOR evReplyRejestr()

#define evKeyUp_SERIALIZE OM_NO_OP

#define evKeyUp_UNSERIALIZE OM_NO_OP

#define evKeyUp_CONSTRUCTOR evKeyUp()
//#]

//## package Default


#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */);

IMPLEMENT_META_PACKAGE(Default, Default)

static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */) {
}
#endif // _OMINSTRUMENT

//## event evZmiana()
evZmiana::evZmiana() {
    NOTIFY_EVENT_CONSTRUCTOR(evZmiana)
    setId(evZmiana_Default_id);
}

bool evZmiana::isTypeOf(const short id) const {
    return (evZmiana_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evZmiana, Default, Default, evZmiana())

//## event evRedukujG()
evRedukujG::evRedukujG() {
    NOTIFY_EVENT_CONSTRUCTOR(evRedukujG)
    setId(evRedukujG_Default_id);
}

bool evRedukujG::isTypeOf(const short id) const {
    return (evRedukujG_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evRedukujG, Default, Default, evRedukujG())

//## event evStop()
evStop::evStop() {
    NOTIFY_EVENT_CONSTRUCTOR(evStop)
    setId(evStop_Default_id);
}

bool evStop::isTypeOf(const short id) const {
    return (evStop_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evStop, Default, Default, evStop())

//## event evZadajTemp()
evZadajTemp::evZadajTemp() {
    NOTIFY_EVENT_CONSTRUCTOR(evZadajTemp)
    setId(evZadajTemp_Default_id);
}

bool evZadajTemp::isTypeOf(const short id) const {
    return (evZadajTemp_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evZadajTemp, Default, Default, evZadajTemp())

//## event evStart()
evStart::evStart() {
    NOTIFY_EVENT_CONSTRUCTOR(evStart)
    setId(evStart_Default_id);
}

bool evStart::isTypeOf(const short id) const {
    return (evStart_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evStart, Default, Default, evStart())

//## event evZwiekszG()
evZwiekszG::evZwiekszG() {
    NOTIFY_EVENT_CONSTRUCTOR(evZwiekszG)
    setId(evZwiekszG_Default_id);
}

bool evZwiekszG::isTypeOf(const short id) const {
    return (evZwiekszG_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evZwiekszG, Default, Default, evZwiekszG())

//## event evSprawdzStan()
evSprawdzStan::evSprawdzStan() {
    NOTIFY_EVENT_CONSTRUCTOR(evSprawdzStan)
    setId(evSprawdzStan_Default_id);
}

bool evSprawdzStan::isTypeOf(const short id) const {
    return (evSprawdzStan_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evSprawdzStan, Default, Default, evSprawdzStan())

//## event evGetTemp()
evGetTemp::evGetTemp() {
    NOTIFY_EVENT_CONSTRUCTOR(evGetTemp)
    setId(evGetTemp_Default_id);
}

bool evGetTemp::isTypeOf(const short id) const {
    return (evGetTemp_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evGetTemp, Default, Default, evGetTemp())

//## event evCzujnikKaput()
evCzujnikKaput::evCzujnikKaput() {
    NOTIFY_EVENT_CONSTRUCTOR(evCzujnikKaput)
    setId(evCzujnikKaput_Default_id);
}

bool evCzujnikKaput::isTypeOf(const short id) const {
    return (evCzujnikKaput_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evCzujnikKaput, Default, Default, evCzujnikKaput())

//## event evResetujStan()
evResetujStan::evResetujStan() {
    NOTIFY_EVENT_CONSTRUCTOR(evResetujStan)
    setId(evResetujStan_Default_id);
}

bool evResetujStan::isTypeOf(const short id) const {
    return (evResetujStan_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evResetujStan, Default, Default, evResetujStan())

//## event evZmienCzas()
evZmienCzas::evZmienCzas() {
    NOTIFY_EVENT_CONSTRUCTOR(evZmienCzas)
    setId(evZmienCzas_Default_id);
}

bool evZmienCzas::isTypeOf(const short id) const {
    return (evZmienCzas_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evZmienCzas, Default, Default, evZmienCzas())

//## event evZwiekszTemp()
evZwiekszTemp::evZwiekszTemp() {
    NOTIFY_EVENT_CONSTRUCTOR(evZwiekszTemp)
    setId(evZwiekszTemp_Default_id);
}

bool evZwiekszTemp::isTypeOf(const short id) const {
    return (evZwiekszTemp_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evZwiekszTemp, Default, Default, evZwiekszTemp())

//## event evZmniejszTemp()
evZmniejszTemp::evZmniejszTemp() {
    NOTIFY_EVENT_CONSTRUCTOR(evZmniejszTemp)
    setId(evZmniejszTemp_Default_id);
}

bool evZmniejszTemp::isTypeOf(const short id) const {
    return (evZmniejszTemp_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evZmniejszTemp, Default, Default, evZmniejszTemp())

//## event evGetTime()
evGetTime::evGetTime() {
    NOTIFY_EVENT_CONSTRUCTOR(evGetTime)
    setId(evGetTime_Default_id);
}

bool evGetTime::isTypeOf(const short id) const {
    return (evGetTime_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evGetTime, Default, Default, evGetTime())

//## event evKeyUpPressed()
evKeyUpPressed::evKeyUpPressed() {
    NOTIFY_EVENT_CONSTRUCTOR(evKeyUpPressed)
    setId(evKeyUpPressed_Default_id);
}

bool evKeyUpPressed::isTypeOf(const short id) const {
    return (evKeyUpPressed_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evKeyUpPressed, Default, Default, evKeyUpPressed())

//## event evAutoTempKey()
evAutoTempKey::evAutoTempKey() {
    NOTIFY_EVENT_CONSTRUCTOR(evAutoTempKey)
    setId(evAutoTempKey_Default_id);
}

bool evAutoTempKey::isTypeOf(const short id) const {
    return (evAutoTempKey_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evAutoTempKey, Default, Default, evAutoTempKey())

//## event evKeyTimeUp()
evKeyTimeUp::evKeyTimeUp() {
    NOTIFY_EVENT_CONSTRUCTOR(evKeyTimeUp)
    setId(evKeyTimeUp_Default_id);
}

bool evKeyTimeUp::isTypeOf(const short id) const {
    return (evKeyTimeUp_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evKeyTimeUp, Default, Default, evKeyTimeUp())

//## event evKeyRejestr()
evKeyRejestr::evKeyRejestr() {
    NOTIFY_EVENT_CONSTRUCTOR(evKeyRejestr)
    setId(evKeyRejestr_Default_id);
}

bool evKeyRejestr::isTypeOf(const short id) const {
    return (evKeyRejestr_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evKeyRejestr, Default, Default, evKeyRejestr())

//## event evAskRejestr()
evAskRejestr::evAskRejestr() {
    NOTIFY_EVENT_CONSTRUCTOR(evAskRejestr)
    setId(evAskRejestr_Default_id);
}

bool evAskRejestr::isTypeOf(const short id) const {
    return (evAskRejestr_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evAskRejestr, Default, Default, evAskRejestr())

//## event evSaveTime()
evSaveTime::evSaveTime() {
    NOTIFY_EVENT_CONSTRUCTOR(evSaveTime)
    setId(evSaveTime_Default_id);
}

bool evSaveTime::isTypeOf(const short id) const {
    return (evSaveTime_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evSaveTime, Default, Default, evSaveTime())

//## event evAutoKeyPressed()
evAutoKeyPressed::evAutoKeyPressed() {
    NOTIFY_EVENT_CONSTRUCTOR(evAutoKeyPressed)
    setId(evAutoKeyPressed_Default_id);
}

bool evAutoKeyPressed::isTypeOf(const short id) const {
    return (evAutoKeyPressed_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evAutoKeyPressed, Default, Default, evAutoKeyPressed())

//## event evAwaria()
evAwaria::evAwaria() {
    NOTIFY_EVENT_CONSTRUCTOR(evAwaria)
    setId(evAwaria_Default_id);
}

bool evAwaria::isTypeOf(const short id) const {
    return (evAwaria_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evAwaria, Default, Default, evAwaria())

//## event evKeyDownPressed()
evKeyDownPressed::evKeyDownPressed() {
    NOTIFY_EVENT_CONSTRUCTOR(evKeyDownPressed)
    setId(evKeyDownPressed_Default_id);
}

bool evKeyDownPressed::isTypeOf(const short id) const {
    return (evKeyDownPressed_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evKeyDownPressed, Default, Default, evKeyDownPressed())

//## event evKeyTimeDown()
evKeyTimeDown::evKeyTimeDown() {
    NOTIFY_EVENT_CONSTRUCTOR(evKeyTimeDown)
    setId(evKeyTimeDown_Default_id);
}

bool evKeyTimeDown::isTypeOf(const short id) const {
    return (evKeyTimeDown_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evKeyTimeDown, Default, Default, evKeyTimeDown())

//## event evReplyRejestr()
evReplyRejestr::evReplyRejestr() {
    NOTIFY_EVENT_CONSTRUCTOR(evReplyRejestr)
    setId(evReplyRejestr_Default_id);
}

bool evReplyRejestr::isTypeOf(const short id) const {
    return (evReplyRejestr_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evReplyRejestr, Default, Default, evReplyRejestr())

//## event evKeyUp()
evKeyUp::evKeyUp() {
    NOTIFY_EVENT_CONSTRUCTOR(evKeyUp)
    setId(evKeyUp_Default_id);
}

bool evKeyUp::isTypeOf(const short id) const {
    return (evKeyUp_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evKeyUp, Default, Default, evKeyUp())

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default.cpp
*********************************************************************/
