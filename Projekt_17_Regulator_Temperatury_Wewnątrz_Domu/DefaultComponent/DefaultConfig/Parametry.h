/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Parametry
//!	Generated Date	: Tue, 10, Sep 2019  
	File Path	: DefaultComponent/DefaultConfig/Parametry.h
*********************************************************************/

#ifndef Parametry_H
#define Parametry_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## link itsRegulator
class Regulator;

//## package Default

//## class Parametry
class Parametry {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedParametry;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Parametry();
    
    //## auto_generated
    ~Parametry();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Regulator* getItsRegulator() const;
    
    //## auto_generated
    void setItsRegulator(Regulator* p_Regulator);

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Regulator* itsRegulator;		//## link itsRegulator
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsRegulator(Regulator* p_Regulator);
    
    //## auto_generated
    void _setItsRegulator(Regulator* p_Regulator);
    
    //## auto_generated
    void _clearItsRegulator();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedParametry : virtual public AOMInstance {
    DECLARE_META(Parametry, OMAnimatedParametry)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Parametry.h
*********************************************************************/
