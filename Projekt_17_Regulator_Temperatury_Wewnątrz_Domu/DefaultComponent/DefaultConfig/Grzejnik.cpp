/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Grzejnik
//!	Generated Date	: Tue, 10, Sep 2019  
	File Path	: DefaultComponent/DefaultConfig/Grzejnik.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Grzejnik.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define Default_Grzejnik_Grzejnik_SERIALIZE OM_NO_OP

#define Default_Grzejnik_getOption_SERIALIZE OM_NO_OP

#define Default_Grzejnik_off_SERIALIZE OM_NO_OP

#define Default_Grzejnik_on_SERIALIZE OM_NO_OP

#define Default_Grzejnik_setOption_SERIALIZE aomsmethod->addAttribute("option", x2String(option));

#define OMAnim_Default_Grzejnik_setStanG_Stan_UNSERIALIZE_ARGS OP_UNSER(OMDestructiveString2X,(int&)p_StanG)

#define OMAnim_Default_Grzejnik_setStanG_Stan_SERIALIZE_RET_VAL
//#]

//## package Default

//## class Grzejnik
Grzejnik::Grzejnik(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Grzejnik, Grzejnik(), 0, Default_Grzejnik_Grzejnik_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsSterownik = NULL;
    initStatechart();
}

Grzejnik::~Grzejnik() {
    NOTIFY_DESTRUCTOR(~Grzejnik, false);
    cleanUpRelations();
}

RhpString Grzejnik::getOption() {
    NOTIFY_OPERATION(getOption, getOption(), 0, Default_Grzejnik_getOption_SERIALIZE);
    //#[ operation getOption()
    return "x";
    //#]
}

bool Grzejnik::off() {
    NOTIFY_OPERATION(off, off(), 0, Default_Grzejnik_off_SERIALIZE);
    //#[ operation off()
    return true;
    //#]
}

bool Grzejnik::on() {
    NOTIFY_OPERATION(on, on(), 0, Default_Grzejnik_on_SERIALIZE);
    //#[ operation on()
    return true;
    //#]
}

bool Grzejnik::setOption(const RhpString& option) {
    NOTIFY_OPERATION(setOption, setOption(const RhpString&), 1, Default_Grzejnik_setOption_SERIALIZE);
    //#[ operation setOption(RhpString)
    return true;
    //#]
}

Sterownik* Grzejnik::getItsSterownik() const {
    return itsSterownik;
}

void Grzejnik::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool Grzejnik::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void Grzejnik::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
}

void Grzejnik::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

Stan Grzejnik::getStanG() const {
    return StanG;
}

void Grzejnik::setStanG(Stan p_StanG) {
    StanG = p_StanG;
    NOTIFY_SET_OPERATION;
}

int Grzejnik::getTempZadanaG() const {
    return TempZadanaG;
}

void Grzejnik::setTempZadanaG(int p_TempZadanaG) {
    TempZadanaG = p_TempZadanaG;
}

void Grzejnik::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void Grzejnik::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void Grzejnik::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

int Grzejnik::getTempBiezG() const {
    return TempBiezG;
}

void Grzejnik::setTempBiezG(int p_TempBiezG) {
    TempBiezG = p_TempBiezG;
}

void Grzejnik::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.OFF");
        rootState_subState = OFF;
        rootState_active = OFF;
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Grzejnik::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State OFF
        case OFF:
        {
            if(IS_EVENT_TYPE_OF(evStart_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("1");
                    NOTIFY_STATE_EXITED("ROOT.OFF");
                    NOTIFY_STATE_ENTERED("ROOT.ON");
                    pushNullTransition();
                    rootState_subState = ON;
                    rootState_active = ON;
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            
        }
        break;
        // State ON
        case ON:
        {
            if(IS_EVENT_TYPE_OF(evStop_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("2");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.ON");
                    NOTIFY_STATE_ENTERED("ROOT.OFF");
                    rootState_subState = OFF;
                    rootState_active = OFF;
                    NOTIFY_TRANSITION_TERMINATED("2");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 3 
                    if(TempZadanaG>=TempBiezG)
                        {
                            NOTIFY_TRANSITION_STARTED("3");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.ON");
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_4");
                            pushNullTransition();
                            rootState_subState = sendaction_4;
                            rootState_active = sendaction_4;
                            //#[ state sendaction_4.(Entry) 
                            GEN(evZwiekszG);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("3");
                            res = eventConsumed;
                        }
                    else
                        {
                            //## transition 4 
                            if(TempZadanaG<TempBiezG)
                                {
                                    NOTIFY_TRANSITION_STARTED("4");
                                    popNullTransition();
                                    NOTIFY_STATE_EXITED("ROOT.ON");
                                    NOTIFY_STATE_ENTERED("ROOT.sendaction_5");
                                    pushNullTransition();
                                    rootState_subState = sendaction_5;
                                    rootState_active = sendaction_5;
                                    //#[ state sendaction_5.(Entry) 
                                    GEN(evRedukujG);
                                    //#]
                                    NOTIFY_TRANSITION_TERMINATED("4");
                                    res = eventConsumed;
                                }
                        }
                }
            
        }
        break;
        // State sendaction_4
        case sendaction_4:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("5");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_4");
                    NOTIFY_STATE_ENTERED("ROOT.sendaction_6");
                    pushNullTransition();
                    rootState_subState = sendaction_6;
                    rootState_active = sendaction_6;
                    //#[ state sendaction_6.(Entry) 
                    itsSterownik->GEN(evGetTemp);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("5");
                    res = eventConsumed;
                }
            
        }
        break;
        // State sendaction_5
        case sendaction_5:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("6");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_5");
                    NOTIFY_STATE_ENTERED("ROOT.sendaction_6");
                    pushNullTransition();
                    rootState_subState = sendaction_6;
                    rootState_active = sendaction_6;
                    //#[ state sendaction_6.(Entry) 
                    itsSterownik->GEN(evGetTemp);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("6");
                    res = eventConsumed;
                }
            
        }
        break;
        // State sendaction_6
        case sendaction_6:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("7");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_6");
                    NOTIFY_STATE_ENTERED("ROOT.ON");
                    pushNullTransition();
                    rootState_subState = ON;
                    rootState_active = ON;
                    NOTIFY_TRANSITION_TERMINATED("7");
                    res = eventConsumed;
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedGrzejnik::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("StanG", x2String((int)myReal->StanG));
    aomsAttributes->addAttribute("TempZadanaG", x2String(myReal->TempZadanaG));
    aomsAttributes->addAttribute("TempBiezG", x2String(myReal->TempBiezG));
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedGrzejnik::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}

void OMAnimatedGrzejnik::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Grzejnik::OFF:
        {
            OFF_serializeStates(aomsState);
        }
        break;
        case Grzejnik::ON:
        {
            ON_serializeStates(aomsState);
        }
        break;
        case Grzejnik::sendaction_4:
        {
            sendaction_4_serializeStates(aomsState);
        }
        break;
        case Grzejnik::sendaction_5:
        {
            sendaction_5_serializeStates(aomsState);
        }
        break;
        case Grzejnik::sendaction_6:
        {
            sendaction_6_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedGrzejnik::sendaction_6_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_6");
}

void OMAnimatedGrzejnik::sendaction_5_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_5");
}

void OMAnimatedGrzejnik::sendaction_4_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_4");
}

void OMAnimatedGrzejnik::ON_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.ON");
}

void OMAnimatedGrzejnik::OFF_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.OFF");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(Grzejnik, Default, false, Modul, OMAnimatedModul, OMAnimatedGrzejnik)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS

IMPLEMENT_META_OP(OMAnimatedGrzejnik, Default_Grzejnik_setStanG_Stan, "setStanG", FALSE, "setStanG(Stan)", 1)

IMPLEMENT_OP_CALL(Default_Grzejnik_setStanG_Stan, Grzejnik, setStanG(p_StanG), NO_OP())
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Grzejnik.cpp
*********************************************************************/
