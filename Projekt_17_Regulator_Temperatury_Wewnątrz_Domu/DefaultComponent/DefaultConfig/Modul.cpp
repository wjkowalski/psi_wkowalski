/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Modul
//!	Generated Date	: Tue, 10, Sep 2019  
	File Path	: DefaultComponent/DefaultConfig/Modul.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Modul.h"
//#[ ignore
#define Default_Modul_Modul_SERIALIZE OM_NO_OP

#define Default_Modul_getOption_SERIALIZE OM_NO_OP

#define Default_Modul_off_SERIALIZE OM_NO_OP

#define Default_Modul_on_SERIALIZE OM_NO_OP

#define Default_Modul_setOption_SERIALIZE aomsmethod->addAttribute("option", x2String(option));
//#]

//## package Default

//## class Modul
Modul::Modul() {
    NOTIFY_CONSTRUCTOR(Modul, Modul(), 0, Default_Modul_Modul_SERIALIZE);
}

Modul::~Modul() {
    NOTIFY_DESTRUCTOR(~Modul, true);
}

RhpString Modul::getOption() {
    NOTIFY_OPERATION(getOption, getOption(), 0, Default_Modul_getOption_SERIALIZE);
    //#[ operation getOption()
    return "x";
    //#]
}

bool Modul::off() {
    NOTIFY_OPERATION(off, off(), 0, Default_Modul_off_SERIALIZE);
    //#[ operation off()
    return true;
    //#]
}

bool Modul::on() {
    NOTIFY_OPERATION(on, on(), 0, Default_Modul_on_SERIALIZE);
    //#[ operation on()
    return true;
    //#]
}

bool Modul::setOption(const RhpString& option) {
    NOTIFY_OPERATION(setOption, setOption(const RhpString&), 1, Default_Modul_setOption_SERIALIZE);
    //#[ operation setOption(RhpString)
    return true;
    //#]
}

int Modul::getID() const {
    return ID;
}

void Modul::setID(int p_ID) {
    ID = p_ID;
}

bool Modul::getAvalible() const {
    return avalible;
}

void Modul::setAvalible(bool p_avalible) {
    avalible = p_avalible;
}

RhpString Modul::getName() const {
    return name;
}

void Modul::setName(RhpString p_name) {
    name = p_name;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedModul::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("ID", x2String(myReal->ID));
    aomsAttributes->addAttribute("avalible", x2String(myReal->avalible));
    aomsAttributes->addAttribute("name", x2String(myReal->name));
}
//#]

IMPLEMENT_META_P(Modul, Default, Default, false, OMAnimatedModul)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Modul.cpp
*********************************************************************/
