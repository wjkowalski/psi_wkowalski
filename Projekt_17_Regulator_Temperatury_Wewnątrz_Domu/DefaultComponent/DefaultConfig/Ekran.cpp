/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Ekran
//!	Generated Date	: Sun, 8, Sep 2019  
	File Path	: DefaultComponent\DefaultConfig\Ekran.cpp
*********************************************************************/

//## auto_generated
#include <oxf\omthread.h>
//## auto_generated
#include "Ekran.h"
//## link itsSterownik
#include "Sterownik.h"
//## package Default

//## class Ekran
Ekran::Ekran(IOxfActive* theActiveContext) {
    setActiveContext(theActiveContext, false);
    itsSterownik = NULL;
    initStatechart();
}

Ekran::~Ekran() {
    cleanUpRelations();
}

Sterownik* Ekran::getItsSterownik() const {
    return itsSterownik;
}

void Ekran::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool Ekran::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void Ekran::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
}

void Ekran::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            itsSterownik = NULL;
        }
}

void Ekran::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
}

void Ekran::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void Ekran::_clearItsSterownik() {
    itsSterownik = NULL;
}

void Ekran::rootState_entDef() {
}

IOxfReactive::TakeEventStatus Ekran::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    return res;
}

/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Ekran.cpp
*********************************************************************/
