/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Sterownik
//!	Generated Date	: Tue, 10, Sep 2019  
	File Path	: DefaultComponent/DefaultConfig/Sterownik.h
*********************************************************************/

#ifndef Sterownik_H
#define Sterownik_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## link itsCzujnik
#include "Czujnik.h"
//## link itsGrzejnik
#include "Grzejnik.h"
//## classInstance itsPanelKontrolny
#include "PanelKontrolny.h"
//## classInstance itsRejestr
#include "Rejestr.h"
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include <oxf/omthread.h>
//#[ ignore
#define OMAnim_Default_Sterownik_setAutozmiana_Stan_ARGS_DECLARATION Stan p_autozmiana;
//#]

//## link itsRegulator
class Regulator;

//## package Default

//## class Sterownik
class Sterownik : public OMReactive {
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedSterownik;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Sterownik(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Sterownik();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Czujnik* getItsCzujnik() const;
    
    //## auto_generated
    Grzejnik* getItsGrzejnik() const;
    
    //## auto_generated
    PanelKontrolny* getItsPanelKontrolny() const;
    
    //## auto_generated
    Regulator* getItsRegulator() const;
    
    //## auto_generated
    void setItsRegulator(Regulator* p_Regulator);
    
    //## auto_generated
    Rejestr* getItsRejestr() const;
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();
    
    //## auto_generated
    void cancelTimeouts();
    
    //## auto_generated
    bool cancelTimeout(const IOxfTimeout* arg);

private :

    //## auto_generated
    int getCzas() const;

public :

    //## auto_generated
    void setCzas(int p_Czas);

private :

    //## auto_generated
    int getCzasZmiany() const;

public :

    //## auto_generated
    void setCzasZmiany(int p_CzasZmiany);

private :

    //## auto_generated
    float getTempBiezS() const;

public :

    //## auto_generated
    void setTempBiezS(float p_TempBiezS);

private :

    //## auto_generated
    float getTempZadana() const;

public :

    //## auto_generated
    void setTempZadana(float p_TempZadana);

private :

    //## auto_generated
    AutomatZmiana getAutooperacja() const;
    
    //## auto_generated
    void setAutooperacja(AutomatZmiana p_autooperacja);
    
    //## auto_generated
    Stan getAutozmiana() const;

public :

    //## auto_generated
    void setAutozmiana(Stan p_autozmiana);

private :

    //## auto_generated
    Operacja getOperacje() const;
    
    //## auto_generated
    void setOperacje(Operacja p_operacje);
    
    ////    Attributes    ////

protected :

    int Czas;		//## attribute Czas
    
    int CzasZmiany;		//## attribute CzasZmiany
    
    float TempBiezS;		//## attribute TempBiezS
    
    float TempZadana;		//## attribute TempZadana
    
    AutomatZmiana autooperacja;		//## attribute autooperacja
    
    Stan autozmiana;		//## attribute autozmiana
    
    Operacja operacje;		//## attribute operacje
    
    ////    Relations and components    ////
    
    Czujnik itsCzujnik;		//## link itsCzujnik
    
    Grzejnik itsGrzejnik;		//## link itsGrzejnik
    
    PanelKontrolny itsPanelKontrolny;		//## classInstance itsPanelKontrolny
    
    Regulator* itsRegulator;		//## link itsRegulator
    
    Rejestr itsRejestr;		//## classInstance itsRejestr
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsRegulator(Regulator* p_Regulator);
    
    //## auto_generated
    void _setItsRegulator(Regulator* p_Regulator);
    
    //## auto_generated
    void _clearItsRegulator();
    
    //## auto_generated
    void setActiveContext(IOxfActive* theActiveContext, bool activeInstance);
    
    //## auto_generated
    virtual void destroy();
    
    ////    Framework    ////
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // ZmianaUp:
    //## statechart_method
    inline bool ZmianaUp_IN() const;
    
    //## statechart_method
    void ZmianaUp_entDef();
    
    //## statechart_method
    void ZmianaUp_exit();
    
    // sendaction_31:
    //## statechart_method
    inline bool sendaction_31_IN() const;
    
    // ZmianaUp_sendaction_12:
    //## statechart_method
    inline bool ZmianaUp_sendaction_12_IN() const;
    
    // ZmianaUp_sendaction_10:
    //## statechart_method
    inline bool ZmianaUp_sendaction_10_IN() const;
    
    // accepteventaction_39:
    //## statechart_method
    inline bool accepteventaction_39_IN() const;
    
    // ZmianaDown:
    //## statechart_method
    inline bool ZmianaDown_IN() const;
    
    //## statechart_method
    void ZmianaDown_entDef();
    
    //## statechart_method
    void ZmianaDown_exit();
    
    // sendaction_37:
    //## statechart_method
    inline bool sendaction_37_IN() const;
    
    // sendaction_36:
    //## statechart_method
    inline bool sendaction_36_IN() const;
    
    // sendaction_35:
    //## statechart_method
    inline bool sendaction_35_IN() const;
    
    // ZmianaDown_accepteventaction_39:
    //## statechart_method
    inline bool ZmianaDown_accepteventaction_39_IN() const;
    
    // StanObecny:
    //## statechart_method
    inline bool StanObecny_IN() const;
    
    //## statechart_method
    void StanObecny_entDef();
    
    // sendaction_18:
    //## statechart_method
    inline bool sendaction_18_IN() const;
    
    // sendaction_16:
    //## statechart_method
    inline bool sendaction_16_IN() const;
    
    // SprawdzenieRejestru:
    //## statechart_method
    inline bool SprawdzenieRejestru_IN() const;
    
    //## statechart_method
    void SprawdzenieRejestru_entDef();
    
    //## statechart_method
    void SprawdzenieRejestru_exit();
    
    // sendaction_28:
    //## statechart_method
    inline bool sendaction_28_IN() const;
    
    // Oczekiwanie:
    //## statechart_method
    inline bool Oczekiwanie_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Oczekiwanie_handleEvent();
    
    // Autozmiana:
    //## statechart_method
    inline bool Autozmiana_IN() const;
    
    //## statechart_method
    void Autozmiana_entDef();
    
    //## statechart_method
    void AutozmianaEntDef();
    
    //## statechart_method
    void Autozmiana_exit();
    
    // sendaction_22:
    //## statechart_method
    inline bool sendaction_22_IN() const;
    
    // sendaction_21:
    //## statechart_method
    inline bool sendaction_21_IN() const;
    
    // sendaction_13:
    //## statechart_method
    inline bool sendaction_13_IN() const;
    
    // sendaction_12:
    //## statechart_method
    inline bool sendaction_12_IN() const;
    
    // sendaction_10:
    //## statechart_method
    inline bool sendaction_10_IN() const;

protected :

//#[ ignore
    enum Sterownik_Enum {
        OMNonState = 0,
        ZmianaUp = 1,
        sendaction_31 = 2,
        ZmianaUp_sendaction_12 = 3,
        ZmianaUp_sendaction_10 = 4,
        accepteventaction_39 = 5,
        ZmianaDown = 6,
        sendaction_37 = 7,
        sendaction_36 = 8,
        sendaction_35 = 9,
        ZmianaDown_accepteventaction_39 = 10,
        StanObecny = 11,
        sendaction_18 = 12,
        sendaction_16 = 13,
        SprawdzenieRejestru = 14,
        sendaction_28 = 15,
        Oczekiwanie = 16,
        Autozmiana = 17,
        sendaction_22 = 18,
        sendaction_21 = 19,
        sendaction_13 = 20,
        sendaction_12 = 21,
        sendaction_10 = 22
    };
    
    int rootState_subState;
    
    int rootState_active;
    
    int ZmianaUp_subState;
    
    int ZmianaDown_subState;
    
    int StanObecny_subState;
    
    int SprawdzenieRejestru_subState;
    
    IOxfTimeout* SprawdzenieRejestru_timeout;
    
    IOxfTimeout* rootState_timeout;
    
    int Autozmiana_subState;
//#]
};

#ifdef _OMINSTRUMENT
DECLARE_OPERATION_CLASS(Default_Sterownik_setAutozmiana_Stan)

//#[ ignore
class OMAnimatedSterownik : virtual public AOMInstance {
    DECLARE_REACTIVE_META(Sterownik, OMAnimatedSterownik)
    
    DECLARE_META_OP(Default_Sterownik_setAutozmiana_Stan)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void ZmianaUp_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_31_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void ZmianaUp_sendaction_12_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void ZmianaUp_sendaction_10_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void accepteventaction_39_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void ZmianaDown_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_37_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_36_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_35_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void ZmianaDown_accepteventaction_39_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void StanObecny_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_18_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_16_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void SprawdzenieRejestru_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_28_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Oczekiwanie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Autozmiana_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_22_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_21_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_13_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_12_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_10_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Sterownik::rootState_IN() const {
    return true;
}

inline bool Sterownik::ZmianaUp_IN() const {
    return rootState_subState == ZmianaUp;
}

inline bool Sterownik::sendaction_31_IN() const {
    return ZmianaUp_subState == sendaction_31;
}

inline bool Sterownik::ZmianaUp_sendaction_12_IN() const {
    return ZmianaUp_subState == ZmianaUp_sendaction_12;
}

inline bool Sterownik::ZmianaUp_sendaction_10_IN() const {
    return ZmianaUp_subState == ZmianaUp_sendaction_10;
}

inline bool Sterownik::accepteventaction_39_IN() const {
    return ZmianaUp_subState == accepteventaction_39;
}

inline bool Sterownik::ZmianaDown_IN() const {
    return rootState_subState == ZmianaDown;
}

inline bool Sterownik::sendaction_37_IN() const {
    return ZmianaDown_subState == sendaction_37;
}

inline bool Sterownik::sendaction_36_IN() const {
    return ZmianaDown_subState == sendaction_36;
}

inline bool Sterownik::sendaction_35_IN() const {
    return ZmianaDown_subState == sendaction_35;
}

inline bool Sterownik::ZmianaDown_accepteventaction_39_IN() const {
    return ZmianaDown_subState == ZmianaDown_accepteventaction_39;
}

inline bool Sterownik::StanObecny_IN() const {
    return rootState_subState == StanObecny;
}

inline bool Sterownik::sendaction_18_IN() const {
    return StanObecny_subState == sendaction_18;
}

inline bool Sterownik::sendaction_16_IN() const {
    return StanObecny_subState == sendaction_16;
}

inline bool Sterownik::SprawdzenieRejestru_IN() const {
    return rootState_subState == SprawdzenieRejestru;
}

inline bool Sterownik::sendaction_28_IN() const {
    return SprawdzenieRejestru_subState == sendaction_28;
}

inline bool Sterownik::Oczekiwanie_IN() const {
    return rootState_subState == Oczekiwanie;
}

inline bool Sterownik::Autozmiana_IN() const {
    return rootState_subState == Autozmiana;
}

inline bool Sterownik::sendaction_22_IN() const {
    return Autozmiana_subState == sendaction_22;
}

inline bool Sterownik::sendaction_21_IN() const {
    return Autozmiana_subState == sendaction_21;
}

inline bool Sterownik::sendaction_13_IN() const {
    return Autozmiana_subState == sendaction_13;
}

inline bool Sterownik::sendaction_12_IN() const {
    return Autozmiana_subState == sendaction_12;
}

inline bool Sterownik::sendaction_10_IN() const {
    return Autozmiana_subState == sendaction_10;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Sterownik.h
*********************************************************************/
